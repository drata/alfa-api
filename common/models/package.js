'use strict';

var _ = require('lodash');

module.exports = function(Package) {

	var db = null;

  Package.getApp(function(err, app) {
    db = app.pgConnection;
  });

	Package.findAll = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/findAllPackages');

      pg.query(query, [], function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Package.savePackage = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.nombre,
        request.body.medidas || 'N/A',
        request.body.cantidad,
        request.body.precio,
        request.body.usuario
      ];

      var query = require('../../queries/savePackage');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Package.deletePackage = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.usuario
      ];

      var query = require('../../queries/deletePackage');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Package.updatePackage = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.nombre,
        request.body.medidas || 'N/A',
        request.body.cantidad,
        request.body.precio,
        request.body.usuario
      ];

      var query = require('../../queries/updatePackage');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };
  
  Package.remoteMethod('updatePackage', {
    http: {verb: 'post', path: '/update'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Package.remoteMethod('deletePackage', {
    http: {verb: 'post', path: '/delete'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Package.remoteMethod('savePackage', {
    http: {verb: 'post', path: '/'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Package.remoteMethod('findAll', {
    http: {verb: 'get', path: '/'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'packages', type: 'array' }
  });

};
