'use strict';

var _ = require('lodash');


module.exports = function(Sale) {

	var db = null;

  Sale.getApp(function(err, app) {
    db = app.pgConnection;
  });

	Sale.findAll = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/findAllSales');

      var queryParams = [];

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Sale.getProducts = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getProductsOfSale');

      var queryParams = [
        request.params.id
      ];

      pg.query(query, queryParams, function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Sale.getPayments = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getPaymentsOfSale');

      var queryParams = [
        request.params.id
      ];

      pg.query(query, queryParams, function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Sale.getCurrentSale = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getCurrentSale');

      var queryParams = [
        request.params.id
      ];

      pg.query(query, queryParams, function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, _.first(resp.rows));
      });
    });
  };

  Sale.generatePayment = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        JSON.stringify(request.body.metodos),
        request.body.usuario
      ];
       
      var query = require('../../queries/generatePayment');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Sale.cancel = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var methods = request.body.methods.length > 0 ? JSON.stringify(request.body.methods) : null;
      var queryParams = [
        request.body.id,
        methods,
        '',
        request.body.usuario
      ];

      var query = require('../../queries/cancelSale');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };
  
  Sale.remoteMethod('cancel', {
    http: {verb: 'post', path: '/cancel'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Sale.remoteMethod('generatePayment', {
    http: {verb: 'post', path: '/generatePayment'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Sale.remoteMethod('getCurrentSale', {
    http: {verb: 'get', path: '/:id'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'sale', type: 'object' }
  });
  
  Sale.remoteMethod('getPayments', {
    http: {verb: 'get', path: '/:id/payments'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'payments', type: 'array' }
  });
  
  Sale.remoteMethod('getProducts', {
    http: {verb: 'get', path: '/:id/products'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'products', type: 'array' }
  });

  Sale.remoteMethod('findAll', {
    http: {verb: 'get', path: '/'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'sales', type: 'array' }
  });

};
