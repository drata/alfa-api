'use strict';

var _ = require('lodash');


module.exports = function(Order) {

	var db = null;

  Order.getApp(function(err, app) {
    db = app.pgConnection;
  });

	Order.findAll = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/findAllOrders');

      pg.query(query, [], function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Order.send = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);
      
      var queryParams = [
        request.body.id_cliente,
        JSON.stringify(request.body.client),
        JSON.stringify(request.body.products),
        request.body.isInvoice,
        request.body.measure,
        request.body.photomontage,
        request.body.comments,
        request.body.user
      ];
       
      var query = require('../../queries/sendOrder');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Order.getDetails = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getDetailsOfOrder');

      var queryParams = [
        request.params.id
      ];

      pg.query(query, queryParams, function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, _.first(resp.rows));
      });
    });
  };

  Order.getProducts = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getProductsOfOrder');

      var queryParams = [
        request.params.id
      ];

      pg.query(query, queryParams, function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Order.getBudgets = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getBudgetsOfOrder');

      var queryParams = [
        request.params.id
      ];

      pg.query(query, queryParams, function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Order.getOrderById = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getOrderById');

      var queryParams = [
        request.params.id
      ];

      pg.query(query, queryParams, function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, _.first(resp.rows));
      });
    });
  };

  Order.fastSale = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);
      
      var queryParams = [
        request.body.id_cliente,
        JSON.stringify(request.body.client),
        JSON.stringify(request.body.products),
        JSON.stringify(request.body.methods),
        request.body.isInvoice,
        request.body.measure,
        request.body.photomontage,
        request.body.comments,
        request.body.type,
        request.body.paymentDate,
        request.body.user
      ];
       
      var query = require('../../queries/sendFastSale');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };
  
  Order.remoteMethod('fastSale', {
    http: {verb: 'post', path: '/fastSale'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Order.remoteMethod('getOrderById', {
    http: {verb: 'get', path: '/:id'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'order', type: 'object' }
  });

  Order.remoteMethod('getBudgets', {
    http: {verb: 'get', path: '/:id/budgets'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'budgets', type: 'array' }
  });

  Order.remoteMethod('getProducts', {
    http: {verb: 'get', path: '/:id/products'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'products', type: 'array' }
  });

  Order.remoteMethod('getDetails', {
    http: {verb: 'get', path: '/:id/details'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'details', type: 'object' }
  });
  
  Order.remoteMethod('send', {
    http: {verb: 'post', path: '/send'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Order.remoteMethod('findAll', {
    http: {verb: 'get', path: '/'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'orders', type: 'array' }
  });

};
