'use strict';

var _ = require('lodash');

module.exports = function(Pos) {

	var db = null;

  Pos.getApp(function(err, app) {
    db = app.pgConnection;
  });

	Pos.findAllCuts = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/findAllCuts');

      pg.query(query, [], function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Pos.getPosData = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getPosData');

      pg.query(query, [], function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, _.first(resp.rows));
      });
    });
  };

  Pos.getCurrentCut = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getPosData');

      //Obtener id del corte activo
      pg.query(query, [], function(err, resp) {
        
        if(err) {
          pg.end();
          return fn(err);
        }

        var cut = _.first(resp.rows);
        var query = require('../../queries/getCutById');

        //Obtener datos de corte actual
        pg.query(query, [cut.id_corte], function(err, resp) {

          if(err) {
            pg.end();
            return fn(err);
          }

          var currentCut =  _.first(resp.rows);

          if(!currentCut) {
            pg.end();
            return fn(null,{});
          }
          
          var query = require('../../queries/getTotalsOfCut');

          //Anexar totales
          pg.query(query, [cut.id_corte], function(err, resp) {
            if(err) {
              pg.end();
              return fn(err);
            }
            pg.end();
            var totals =  _.first(resp.rows);
            currentCut.totals = {
              saldo_caja: totals.tsaldo_caja,
              abonos_caja: totals.tabonos_caja,
              cargos_caja: totals.tcargos_caja,
              cargos: totals.tcargos,
              abonos: totals.tabono,
            }
            return fn(null,currentCut);
          });

        });
      });
    });
  };

  Pos.saveCut = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.fecha,
        request.body.saldo_apertura,
        request.body.comentarios,
        request.body.id_usuario
      ];

      var query = require('../../queries/saveCut');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Pos.findAllMoves = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/findAllMoves');

      pg.query(query, [], function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Pos.saveMove = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        0,
        request.body.id_proveedor,
        request.body.concepto,
        request.body.importe,
        request.body.metodo,
        request.body.tipo,
        request.body.movimiento,
        request.body.id_usuario
      ];

      var query = require('../../queries/saveMove');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Pos.getCutById = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

        var query = require('../../queries/getCutById');

        //Obtener datos de corte actual
        pg.query(query, [request.params.id], function(err, resp) {

          if(err) {
            pg.end();
            return fn(err);
          }

          var currentCut =  _.first(resp.rows);

          if(!currentCut) {
            pg.end();
            return fn(null,{});
          }

          var query = require('../../queries/getTotalsOfCut');

          //Anexar totales
          pg.query(query, [request.params.id], function(err, resp) {
            if(err) {
              pg.end();
              return fn(err);
            }
            pg.end();
            var totals =  _.first(resp.rows);
            currentCut.totals = {
              saldo_caja: totals.tsaldo_caja,
              abonos_caja: totals.tabonos_caja,
              cargos_caja: totals.tcargos_caja,
              cargos: totals.tcargos,
              abonos: totals.tabono,
            }
            return fn(null,currentCut);
          });//QUERY getTotalsOfCut

        });//QUERY getCutById

    });//DB CONNECT
  };

  Pos.closingCut = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.saldo_real,
        request.body.comentarios,
        request.body.id_usuario
      ];

      var query = require('../../queries/closingCut');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Pos.editMove = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.movimiento === 'A' ? 0 : request.body.id_proveedor,
        request.body.concepto,
        request.body.importe,
        request.body.metodo,
        request.body.tipo,
        request.body.movimiento,
        request.body.id_usuario
      ];

      var query = require('../../queries/editMove');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Pos.deleteMove = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.id_usuario
      ];

      var query = require('../../queries/deleteMove');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Pos.getMovesByCut = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getMovesByCut');

      pg.query(query, [request.query.id], function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Pos.remoteMethod('getMovesByCut', {
    http: {verb: 'get', path: '/moves/cut'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'moves', type: 'array' }
  });
  
  Pos.remoteMethod('deleteMove', {
    http: {verb: 'post', path: '/moves/delete'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Pos.remoteMethod('editMove', {
    http: {verb: 'post', path: '/moves/update'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Pos.remoteMethod('closingCut', {
    http: {verb: 'post', path: '/cuts/close'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Pos.remoteMethod('getCutById', {
    http: {verb: 'get', path: '/cuts/:id'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'cut', type: 'object' }
  });
  
  Pos.remoteMethod('saveMove', {
    http: {verb: 'post', path: '/moves/'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Pos.remoteMethod('findAllMoves', {
    http: {verb: 'get', path: '/moves'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'moves', type: 'array' }
  });
  
  Pos.remoteMethod('saveCut', {
    http: {verb: 'post', path: '/cuts/'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Pos.remoteMethod('getCurrentCut', {
    http: {verb: 'get', path: '/cuts/active'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'cut', type: 'object' }
  });


  Pos.remoteMethod('getPosData', {
    http: {verb: 'get', path: '/data'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Pos.remoteMethod('findAllCuts', {
    http: {verb: 'get', path: '/cuts'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'cuts', type: 'array' }
  });

};
