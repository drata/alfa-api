'use strict';

var _ = require('lodash');

module.exports = function(Budget) {

	var db = null;

  Budget.getApp(function(err, app) {
    db = app.pgConnection;
  });

	Budget.findAll = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/findAllBudgets');
      
      //request.query.userId
      //'WHERE asignacion IN (0,$1)  '+

      var queryParams = [
        
      ];

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Budget.claimBudget = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.usuario
      ];
       
      var query = require('../../queries/claimBudget');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };


  Budget.getProducts = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getProductsOfBudget');

      var queryParams = [
        request.params.id
      ];

      pg.query(query, queryParams, function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Budget.deleteBudget = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.usuario
      ];
       
      var query = require('../../queries/deleteBudget');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Budget.updateVariables = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.usuario,
        request.body.flag_factura
      ];
       
      var query = require('../../queries/updateVariablesOfBudget');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Budget.addProduct = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.nombre,
        request.body.medidas,
        request.body.cantidad,
        request.body.margen,
        request.body.tipo_precio,
        request.body.precio,
        request.body.paquete,
        request.body.usuario
      ];

      var query = require('../../queries/addProductToBudget');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Budget.deleteProduct = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id_cotizacion,
        request.body.id_producto,
        request.body.usuario
      ];

      var query = require('../../queries/deleteProductOfBudget');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Budget.saveChangesInProducts = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.flag_factura,
        JSON.stringify(request.body.products),
        request.body.usuario
      ];

      var query = require('../../queries/saveChangesInProductsOfBudget');
      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Budget.getSupplies = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getSuppliesOfBudget');

      var queryParams = [
        request.params.id
      ];

      pg.query(query, queryParams, function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Budget.addSupply = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.id_producto,
        request.body.num_prov,
        request.body.tipo_vta,
        request.body.nombre,
        request.body.medidas,
        request.body.cantidad,
        request.body.precio,
        request.body.usuario
      ];

      var query = require('../../queries/addSupplyToBudget');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Budget.deleteSupply = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id_cotizacion,
        request.body.id_insumo,
        request.body.usuario
      ];

      var query = require('../../queries/deleteSupplyOfBudget');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Budget.getAnswers = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getAnswersOfBudget');

      var queryParams = [
        request.params.id
      ];

      pg.query(query, queryParams, function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Budget.addAnswer = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.tipo,
        request.body.contacto,
        request.body.medio,
        request.body.comentarios,
        request.body.usuario
      ];
      
      var query = require('../../queries/addAnswerToBudget');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Budget.generateSale = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.flag_factura,
        request.body.tipo_vta,
        JSON.stringify(request.body.metodos),
        request.body.usuario
      ];
       
      var query = require('../../queries/generateSale');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Budget.newBudget = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id_pedido,
        0,
        request.body.usuario
      ];

      //request.body.asignacion
       
      var query = require('../../queries/newBudget');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Budget.updateDate = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.fecha_entrega,
        request.body.usuario
      ];
      
      var query = require('../../queries/updateDateOfBudget');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Budget.generatePDF = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.descripcion,
        request.body.tipo,
        request.body.usuario
      ];
      
      var query = require('../../queries/generatePDFOfBudget');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Budget.getLogs = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getLogsOfBudget');

      var queryParams = [
        request.params.id
      ];

      pg.query(query, queryParams, function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };
  
  Budget.remoteMethod('getLogs', {
    http: {verb: 'get', path: '/:id/logs'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'logs', type: 'array' }
  });
  
  Budget.remoteMethod('generatePDF', {
    http: {verb: 'post', path: '/generatePDF'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Budget.remoteMethod('updateDate', {
    http: {verb: 'post', path: '/updateDate'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Budget.remoteMethod('newBudget', {
    http: {verb: 'post', path: '/new'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Budget.remoteMethod('generateSale', {
    http: {verb: 'post', path: '/generateSale'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Budget.remoteMethod('addAnswer', {
    http: {verb: 'post', path: '/addAnswer'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Budget.remoteMethod('getAnswers', {
    http: {verb: 'get', path: '/:id/answers'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'answers', type: 'array' }
  });

  Budget.remoteMethod('deleteSupply', {
    http: {verb: 'post', path: '/deleteSupply'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Budget.remoteMethod('addSupply', {
    http: {verb: 'post', path: '/addSupply'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Budget.remoteMethod('getSupplies', {
    http: {verb: 'get', path: '/:id/supplies'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'supplies', type: 'array' }
  });
  
  Budget.remoteMethod('saveChangesInProducts', {
    http: {verb: 'post', path: '/saveChangesInProducts'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Budget.remoteMethod('deleteProduct', {
    http: {verb: 'post', path: '/deleteProduct'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Budget.remoteMethod('addProduct', {
    http: {verb: 'post', path: '/addProduct'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Budget.remoteMethod('updateVariables', {
    http: {verb: 'post', path: '/updateVariables'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Budget.remoteMethod('deleteBudget', {
    http: {verb: 'post', path: '/delete'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Budget.remoteMethod('getProducts', {
    http: {verb: 'get', path: '/:id/products'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'products', type: 'array' }
  });
  
  Budget.remoteMethod('claimBudget', {
    http: {verb: 'post', path: '/claim'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Budget.remoteMethod('findAll', {
    http: {verb: 'get', path: '/'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'budgets', type: 'array' }
  });

};
