'use strict';

var _ = require('lodash');

module.exports = function(Payment) {

	var db = null;

  Payment.getApp(function(err, app) {
    db = app.pgConnection;
  });

	

  Payment.getPaymentsTypes = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getPaymentsTypes');

      pg.query(query, [], function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  

  Payment.saveType = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.nombre,
        request.body.movimiento,
        request.body.visible,
        request.body.id_usuario
      ];

      var query = require('../../queries/savePaymentsType');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Payment.updateType = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.nombre,
        request.body.movimiento,
        request.body.visible,
        request.body.id_usuario
      ];

      var query = require('../../queries/updatePaymentsType');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Payment.deleteType = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.id_usuario
      ];

      var query = require('../../queries/deletePaymentsType');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };
  
  Payment.remoteMethod('deleteType', {
    http: {verb: 'post', path: '/types/delete'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Payment.remoteMethod('updateType', {
    http: {verb: 'post', path: '/types/update'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Payment.remoteMethod('saveType', {
    http: {verb: 'post', path: '/types'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Payment.remoteMethod('getPaymentsTypes', {
    http: {verb: 'get', path: '/types'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'types', type: 'array' }
  });

};
