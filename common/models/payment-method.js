'use strict';

var _ = require('lodash');

module.exports = function(Paymentmethod) {
	
	var db = null;

  Paymentmethod.getApp(function(err, app) {
    db = app.pgConnection;
  });

	Paymentmethod.findAll = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/findAllPaymentmethods');

      pg.query(query, [], function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Paymentmethod.remoteMethod('findAll', {
    http: {verb: 'get', path: '/'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'paymentMethods', type: 'array' }
  });


};
