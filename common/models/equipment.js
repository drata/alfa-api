'use strict';

var _ = require('lodash');


module.exports = function(Equipment) {

	var db = null;

  Equipment.getApp(function(err, app) {
    db = app.pgConnection;
  });

	Equipment.findAll = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/findAllEquipments');

      pg.query(query, [], function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Equipment.save = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.nombre,
        request.body.tipo,
        request.body.descripcion,
        request.body.fecha_compra,
        request.body.usuario
      ];
       
      var query = require('../../queries/saveEquipment');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Equipment.updateEquipment = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.nombre,
        request.body.tipo,
        request.body.descripcion,
        request.body.fecha_compra,
        request.body.usuario
      ];
       
      var query = require('../../queries/updateEquipment');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Equipment.getLogs = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getLogsOfEquipment');

      var queryParams = [
        request.params.id
      ];

      pg.query(query, queryParams, function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };
  
   Equipment.getServices = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getServicesOfEquipment');

      var queryParams = [
        request.params.id
      ];

      pg.query(query, queryParams, function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

   Equipment.getRepairs = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getRepairsOfEquipment');

      var queryParams = [
        request.params.id
      ];

      pg.query(query, queryParams, function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Equipment.saveRepair = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id_equipo,
        request.body.id_usuario,
        request.body.id_proveedor,
        request.body.descripcion_falla,
        request.body.descripcion_reparacion,
        request.body.total,
        request.body.fecha,
        request.body.flag
      ];
       
      var query = require('../../queries/saveRepairEquipment');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Equipment.finishRepair = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.id_equipo,
        request.body.id_usuario,
        request.body.descripcion_reparacion,
        request.body.total,
        request.body.fecha
      ];
       
      var query = require('../../queries/finishRepairEquipment');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Equipment.saveService = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id_equipo,
        request.body.id_usuario,
        request.body.id_proveedor,
        request.body.descripcion_servicio,
        request.body.total,
        request.body.fecha,
        request.body.flag
      ];
       
      var query = require('../../queries/saveServiceEquipment');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Equipment.finishService = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.id_equipo,
        request.body.id_usuario,
        request.body.total
      ];
       
      var query = require('../../queries/finishServiceEquipment');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Equipment.deleteEquipment = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.usuario
      ];
       
      var query = require('../../queries/deleteEquipment');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };
  
  Equipment.remoteMethod('deleteEquipment', {
    http: {verb: 'post', path: '/delete'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Equipment.remoteMethod('finishService', {
    http: {verb: 'post', path: '/services/finish'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Equipment.remoteMethod('saveService', {
    http: {verb: 'post', path: '/services/add'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });



  Equipment.remoteMethod('finishRepair', {
    http: {verb: 'post', path: '/repairs/finish'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Equipment.remoteMethod('saveRepair', {
    http: {verb: 'post', path: '/repairs/add'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Equipment.remoteMethod('getRepairs', {
    http: {verb: 'get', path: '/:id/repairs'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'repairs', type: 'array' }
  });

  Equipment.remoteMethod('getServices', {
    http: {verb: 'get', path: '/:id/services'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'services', type: 'array' }
  });

  Equipment.remoteMethod('getLogs', {
    http: {verb: 'get', path: '/:id/logs'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'logs', type: 'array' }
  });
  
  Equipment.remoteMethod('updateEquipment', {
    http: {verb: 'post', path: '/update'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  
  Equipment.remoteMethod('save', {
    http: {verb: 'post', path: '/'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Equipment.remoteMethod('findAll', {
    http: {verb: 'get', path: '/'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'equipments', type: 'array' }
  });

};
