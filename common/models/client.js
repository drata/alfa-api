'use strict';

var _ = require('lodash');

module.exports = function(Client) {

	var db = null;

  Client.getApp(function(err, app) {
    db = app.pgConnection;
  });

	Client.findAll = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/findAllClients');

      pg.query(query, [], function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Client.save = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.rfc,
			  request.body.nombre,
			  request.body.apellido_paterno,
			  request.body.apellido_materno,
        request.body.nombre_comercial,
			  request.body.email,
			  request.body.telefono,
			  request.body.celular,
			  request.body.calle,
			  request.body.num_ext,
			  request.body.num_int,
			  request.body.colonia,
			  request.body.entre_calles,
			  request.body.cod_postal,
			  request.body.localidad,
			  request.body.municipio,
			  request.body.estado,
			  request.body.credito,
			  request.body.estatus,
			  request.body.usuario,
      ];
		       
      var query = require('../../queries/saveClient');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Client.updateClient = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.rfc,
        request.body.nombre,
        request.body.apellido_paterno,
        request.body.apellido_materno,
        request.body.nombre_comercial,
        request.body.email,
        request.body.telefono,
        request.body.celular,
        request.body.calle,
        request.body.num_ext,
        request.body.num_int,
        request.body.colonia,
        request.body.entre_calles,
        request.body.cod_postal,
        request.body.localidad,
        request.body.municipio,
        request.body.estado,
        request.body.credito,
        request.body.estatus,
        request.body.usuario,
      ];
       
      var query = require('../../queries/updateClient');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Client.getLogs = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getLogsOfClient');

      var queryParams = [
        request.params.id
      ];

      pg.query(query, queryParams, function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Client.deleteClient = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.usuario
      ];
       
      var query = require('../../queries/deleteClient');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Client.findAllCommon = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/findAllCommonClients');

      pg.query(query, [], function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };
  
  Client.remoteMethod('findAllCommon', {
    http: {verb: 'get', path: '/common'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'clients', type: 'array' }
  });
  
  Client.remoteMethod('deleteClient', {
    http: {verb: 'post', path: '/delete'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Client.remoteMethod('getLogs', {
    http: {verb: 'get', path: '/:id/logs'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'logs', type: 'array' }
  });
  
  Client.remoteMethod('updateClient', {
    http: {verb: 'post', path: '/update'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  
  Client.remoteMethod('save', {
    http: {verb: 'post', path: '/'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Client.remoteMethod('findAll', {
    http: {verb: 'get', path: '/'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'clients', type: 'array' }
  });

};
