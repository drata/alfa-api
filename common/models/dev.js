'use strict';

var _ = require('lodash');


module.exports = function(Dev) {

  var db = null;

  Dev.getApp(function(err, app) {
    db = app.pgConnection;
  });

  Dev.findAll = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/findAllDevs');

      pg.query(query, [], function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Dev.getPaybacks = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getPaybacksOfDev');

      var queryParams = [
        request.params.id
      ];

      pg.query(query, queryParams, function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Dev.generatePayback = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        JSON.stringify(request.body.metodos),
        request.body.usuario
      ];
       
      var query = require('../../queries/generatePayback');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };
  
  Dev.remoteMethod('generatePayback', {
    http: {verb: 'post', path: '/generatePayback'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Dev.remoteMethod('getPaybacks', {
    http: {verb: 'get', path: '/:id/paybacks'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'paybacks', type: 'array' }
  });

  Dev.remoteMethod('findAll', {
    http: {verb: 'get', path: '/'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'devs', type: 'array' }
  });

};
