'use strict';

var _ = require('lodash');

module.exports = function(Provider) {

	var db = null;

  Provider.getApp(function(err, app) {
    db = app.pgConnection;
  });

  Provider.getPayments = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getProvidersPayments');

      pg.query(query, [], function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Provider.savePayment = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id_proveedor,
        JSON.stringify(request.body.methods),
        request.body.concepto,
        request.body.importe,
        request.body.fecha,
        request.body.tipo_vta,
        request.body.id_usuario
      ];
      
      var query = require('../../queries/saveProviderPayment');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Provider.generatePayment = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id_orden,
        JSON.stringify(request.body.methods),
        request.body.id_usuario
      ];
      
      var query = require('../../queries/generateProviderPayment');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Provider.deletePayment = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.id_usuario
      ];
      
      var query = require('../../queries/deleteProviderPayment');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Provider.remoteMethod('deletePayment', {
    http: {verb: 'post', path: '/payments/delete'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Provider.remoteMethod('generatePayment', {
    http: {verb: 'post', path: '/payments/generate'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Provider.remoteMethod('savePayment', {
    http: {verb: 'post', path: '/payments/'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Provider.remoteMethod('getPayments', {
    http: {verb: 'get', path: '/payments'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'payments', type: 'array' }
  });

};
