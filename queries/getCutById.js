module.exports='SELECT c.id,c.id_usuario,c.saldo_apertura,c.saldo_cierre,c.diferencia,c.estatus,c.fecha::text AS fecha,c.registro::text AS registro, '+
"COALESCE(u.name||' '||u.last_name,'SIN ASIGNAR') AS nombre_usuario "+
'FROM cortes AS c '+
'LEFT JOIN users AS u ON(c.id_usuario = u.id) '+
"WHERE c.id=$1 LIMIT 1;";

