module.exports="SELECT r.id,r.id_equipo,i.nombre AS nombre_equipo,r.id_usuario,u.name||' '||u.last_name AS nombre_usuario, "+
'r.id_proveedor,p.nombre AS nombre_proveedor,r.descripcion_falla,r.descripcion_reparacion, '+
'r.id_usuario_rep, r.total, r.fecha::text AS fecha, r.registro:: text AS registro, r.flag '+
'FROM reparaciones_equipos AS r '+
'LEFT JOIN users AS u ON (r.id_usuario = u.id) '+
'LEFT JOIN inventario_equipos AS i ON(i.id = r.id_equipo) '+
'LEFT JOIN proveedores AS p ON(p.id = r.id_proveedor) '+
'WHERE r.id_equipo =$1 ORDER BY flag,registro DESC';