module.exports= 'SELECT c.id,c.id_pedido,c.comentarios,c.flag_factura,c.estatus,c.fecha_entrega::text AS fecha_entrega, '+
"c.asignacion,COALESCE(a.name||' '||a.last_name,'SIN ASIGNAR') as nombre_asignacion," +
"c.id_usuario,u.name||' '||u.last_name AS nombre_usuario,c.registro::text AS registro "+ 
'FROM cotizaciones AS c '+
'LEFT JOIN users AS u ON(u.id = c.id_usuario) '+
'LEFT JOIN users AS a ON(a.id = c.asignacion) '+
'WHERE c.id_pedido = $1;';