module.exports="SELECT s.id,s.id_equipo,i.nombre AS nombre_equipo,s.id_usuario,u.name||' '||u.last_name AS nombre_usuario, "+
's.id_proveedor,p.nombre AS nombre_proveedor,s.descripcion_servicio,s.id_usuario_serv,s.total, s.fecha, s.registro:: text AS registro,s.flag, '+
'CASE WHEN (now()::date) > (s.fecha::date) THEN false ELSE true END AS flag_tiempo '+
'FROM servicios_equipos AS s '+
'LEFT JOIN users AS u ON (s.id_usuario = u.id) '+
'LEFT JOIN inventario_equipos AS i ON(i.id = s.id_equipo) '+
'LEFT JOIN proveedores AS p ON(p.id = s.id_proveedor) '+
'WHERE s.id_equipo =$1 ORDER BY flag,registro DESC;';