module.exports = 'SELECT id,trim(rfc) AS rfc,nombre,apellido_paterno,apellido_materno,nombre_comercial,email,telefono,celular,calle,num_ext, '+
'num_int,colonia,entre_calles,cod_postal,localidad,municipio,estado,credito,estatus,registro::text as registro,flag, '+
"nombre||' '||apellido_paterno||' '||COALESCE(apellido_materno,'') as nombre_completo "+
'FROM clientes WHERE flag=true ORDER BY id DESC;'; 