module.exports= 'SELECT c.id,c.id_pedido,c.comentarios,c.flag_factura,c.estatus,c.asignacion,c.id_usuario,c.fecha_entrega::text AS fecha_entrega, '+
"COALESCE(u.name||' '||u.last_name,'SIN ASIGNAR') AS nombre_usuario,c.registro::text AS registro, "+
"p.id_cliente,p.rfc,p.nombre||' '||p.apellido_paterno||' '||COALESCE(p.apellido_materno,'') AS nombre_cliente "+
'FROM cotizaciones AS c '+
'LEFT JOIN users AS u ON(u.id = c.asignacion) '+
'LEFT JOIN pedido_contacto p ON (p.id_pedido = c.id_pedido) '+
'ORDER BY id DESC; ';