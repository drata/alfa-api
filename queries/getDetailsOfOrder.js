module.exports= 'SELECT id,id_pedido,id_cliente,rfc,nombre,apellido_paterno,apellido_materno,email, '+
'telefono,celular,calle,num_ext,num_int,colonia,entre_calles,cod_postal,localidad,municipio,estado,registro::text AS registro '+
'FROM pedido_contacto '+
'WHERE id_pedido = $1;';