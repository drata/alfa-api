module.exports='SELECT id,id_cotizacion,id_pedido,id_cliente,rfc,nombre_cliente,email,telefono,celular,calle, '+
'num_ext,num_int,colonia,entre_calles,cod_postal,localidad,municipio,estado,tipo_vta,subtotal, '+
'descuento,iva,total,saldo,fecha_vigencia,flag_factura,(SELECT sum(importe) FROM movimientos WHERE documento=$1) AS abonos,estatus,registro::text AS registro '+ 
'FROM notas '+
'WHERE id=$1;'