module.exports = 'SELECT i.id,i.nombre,i.tipo,i.descripcion,i.fecha_compra::text AS fecha_compra,i.flag,i.registro::text AS registro, '+
'max(r.fecha) AS ultima_reparacion, max(s.fecha) AS ultimo_servicio '+
'FROM inventario_equipos AS i '+
'LEFT JOIN reparaciones_equipos AS r ON(r.id_equipo = i.id) '+
'LEFT JOIN servicios_equipos AS s ON(s.id_equipo = i.id) '+
'WHERE i.flag = true '+
'GROUP BY  i.id ORDER BY i.id DESC;'; 