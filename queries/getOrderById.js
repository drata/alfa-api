module.exports = 'SELECT p.id,p.id_cliente, '+
"c.nombre||' '||c.apellido_paterno||' '||COALESCE(c.apellido_materno,'') AS nombre_cliente, "+
'p.flag_factura,p.flag_medida,p.flag_montaje,p.comentarios, '+
"p.subtotal,p.descuento,p.iva,p.total,p.estatus,p.origen,p.id_usuario,u.name||' '||u.last_name AS nombre_usuario, "+
'count(t.id) AS cotizaciones, p.registro::text AS registro '+
'FROM pedidos AS p '+
'LEFT JOIN users AS u ON(u.id = p.id_usuario) '+
'LEFT JOIN pedido_contacto AS c ON(c.id_pedido = p.id) '+
'LEFT JOIN cotizaciones AS t ON(t.id_pedido = p.id) '+
'WHERE p.id= $1 '+
'GROUP BY p.id,c.nombre,c.apellido_paterno,c.apellido_materno,u.name,u.last_name '+
'ORDER BY registro DESC;';