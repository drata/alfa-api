module.exports = "SELECT l.id,l.id_equipo,l.usuario,u.name||' '||u.last_name AS nombre_usuario, "+
'l.descripcion,l.tipo,l.registro::text '+
'FROM logs_equipos AS l '+
'LEFT JOIN users AS u ON (l.usuario = u.id) '+
'WHERE l.id_equipo =$1 ORDER BY registro DESC LIMIT 1000';