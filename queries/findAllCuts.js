module.exports="SELECT c.id,c.id_usuario,u.name||' '||u.last_name AS nombre_usuario,c.saldo_apertura, "+
'CASE WHEN a.id_corte > 0 THEN true ELSE false END AS flag_apertura, '+
'CASE WHEN t.id_corte > 0 THEN true ELSE false END AS flag_cierre, '+
'c.saldo_cierre,c.diferencia,c.estatus,c.fecha::text AS fecha,c.registro::text AS registro '+
'FROM cortes AS c '+
'LEFT JOIN users AS u ON (c.id_usuario = u.id) '+
'LEFT JOIN incidencias_apertura_corte AS a ON (c.id = a.id_corte) '+
'LEFT JOIN incidencias_cierre_corte AS t ON (c.id = t.id_corte) '+
'ORDER BY c.id DESC;';