module.exports='SELECT l.id,l.id_cotizacion,l.id_usuario,l.descripcion,l.tipo,l.registro::text AS registro, '+
"u.name||' '||u.last_name AS nombre_usuario,u.name "+
'FROM logs_cotizacion AS l '+
'LEFT JOIN users AS u ON (l.id_usuario = u.id) '+
'WHERE l.id_cotizacion = $1 ORDER BY l.id DESC LIMIT 1000;';