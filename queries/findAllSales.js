module.exports='SELECT n.id,n.id_cliente,n.nombre_cliente,n.tipo_vta,n.total,n.saldo,n.flag_factura, '+
'n.estatus,fecha_vigencia::text AS fecha_vigencia,n.registro::text AS registro '+
'FROM notas AS n  ORDER BY n.id DESC;';


/**
 * 
 module.exports='SELECT n.id,n.id_cotizacion,n.id_pedido,n.id_cliente,n.rfc,n.nombre_cliente,n.email, '+
'n.telefono,n.celular,n.calle,n.num_ext,n.num_int,n.colonia,n.entre_calles,n.cod_postal, '+
'n.localidad,n.municipio,n.estado,n.tipo_vta,n.subtotal,n.iva,n.total,n.saldo,n.flag_factura, '+
'n.estatus,count(d.id),(SELECT sum(importe) FROM movimientos WHERE documento=n.id) AS abonos,n.registro::text AS registro '+
'FROM notas AS n '+
'LEFT JOIN nota_detalle AS d ON(n.id = d.id_nota) '+
'LEFT JOIN movimientos AS m ON (m.documento = n.id) '+
'GROUP BY n.id ORDER BY n.id DESC;';
 */