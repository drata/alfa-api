
CREATE OR REPLACE FUNCTION public.editar_movimiento(
    IN id_movimiento integer,
    IN iproveedor integer,
    IN iconcepto character varying,
    IN iimporte numeric,
    IN imetodo integer,
    IN itipo integer,
    IN imovimiento character,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2019-01-16
-- Descripcion General: Editar registro en movimientos
*****************************************************************************/ 

DECLARE

--Variables de trabajo
data_corte RECORD;
fecha_servidor date;
mov text;
row_mov RECORD;

  
BEGIN
  --Variables de salida
  flag          := false;
  mensaje       := 'Error al editar movimiento ';

  SELECT (now() AT TIME ZONE 'America/Mazatlan')::date INTO fecha_servidor;
  SELECT z.fecha_corte,z.id_corte,z.estado,z.mensaje INTO data_corte FROM obtener_corte_actual() AS z;

  SELECT id_corte INTO row_mov FROM movimientos WHERE id=id_movimiento;

  IF data_corte.estado IN ('A','D') AND row_mov.id_corte = data_corte.id_corte THEN
  
    --Se actualiza movimiento
   UPDATE movimientos SET id_proveedor=iproveedor,id_usuario=iusuario, concepto=iconcepto, importe=iimporte, metodo=imetodo, tipo=itipo, movimiento=imovimiento
   WHERE id=id_movimiento;

    --Se genera log
    INSERT INTO logs_movimientos(id_movimiento,id_usuario,descripcion,tipo)
    VALUES(id_movimiento,iusuario,'Edito la información','U');

    flag          := true;
    mensaje       := 'Se edito el movimiento correctamente';
    RETURN NEXT;
  ELSE
    flag          := false;
    mensaje       := 'Error, el movimiento no pertenece al corte activo';
    RETURN NEXT;
  END IF;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  id_movimiento   := 0;
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;

  CREATE OR REPLACE FUNCTION public.eliminar_movimiento(
    IN id_movimiento integer,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2019-01-16
-- Descripcion General: Eliminar registro en movimientos
*****************************************************************************/ 

DECLARE

--Variables de trabajo
data_corte RECORD;
fecha_servidor date;
mov text;
row_mov RECORD;
  
BEGIN
  --Variables de salida
  flag          := false;
  mensaje       := 'Error al eliminar movimiento ';

  SELECT (now() AT TIME ZONE 'America/Mazatlan')::date INTO fecha_servidor;
  SELECT z.fecha_corte,z.id_corte,z.estado,z.mensaje INTO data_corte FROM obtener_corte_actual() AS z;

  SELECT id_corte INTO row_mov FROM movimientos WHERE id=id_movimiento;

  IF data_corte.estado IN ('A','D') AND row_mov.id_corte = data_corte.id_corte THEN
  
    --Se actualiza movimiento
   UPDATE movimientos SET estatus='E'WHERE id=id_movimiento;

    --Se genera log
    INSERT INTO logs_movimientos(id_movimiento,id_usuario,descripcion,tipo)
    VALUES(id_movimiento,iusuario,'Elimino el movimiento','D');

    flag          := true;
    mensaje       := 'Se elimino el movimiento correctamente';
    RETURN NEXT;
  ELSE
    flag          := false;
    mensaje       := 'Error, el movimiento no pertenece al corte activo';
    RETURN NEXT;
  END IF;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  id_movimiento   := 0;
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;
/** -------------------- EQUIPOS  -------------------- **/


CREATE OR REPLACE FUNCTION agregar_paquete(
    IN inombre character varying,
    IN imedidas character varying,
    IN icantidad integer,
    IN iprecio numeric(10,2),
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-12-17
-- Descripcion General: Agregar registro a paquetes
*****************************************************************************/ 

DECLARE

--Variables de trabajo
id_nuevo_paquete integer;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al guardar paquete ';

  --Se inserta el equipo
  INSERT INTO paquetes(nombre,medidas,cantidad,precio) VALUES(inombre,imedidas,icantidad,iprecio) RETURNING id INTO id_nuevo_paquete;


  --Se genera log
  INSERT INTO logs_paquetes(id_paquete,usuario,descripcion,tipo) VALUES(id_nuevo_paquete,iusuario,'Creo el paquete: '||inombre,'C');

  flag       := true;
  mensaje    := 'Se guardo el paquete correctamente';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER; 


  
CREATE OR REPLACE FUNCTION eliminar_paquete(
    IN ipaquete integer,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-12-17
-- Descripcion General: eliminar registro a paquetes
*****************************************************************************/ 

DECLARE


BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al eliminar paquete ';

  --Se inserta el equipo
  UPDATE paquetes SET flag=false WHERE id=ipaquete;


  --Se genera log
  INSERT INTO logs_paquetes(id_paquete,usuario,descripcion,tipo) VALUES(ipaquete,iusuario,'Elimino el paquete','D');

  flag       := true;
  mensaje    := 'Se elimino el paquete correctamente';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER; 


  CREATE OR REPLACE FUNCTION editar_paquete(
    IN ipaquete integer,
    IN inombre character varying,
    IN imedidas character varying,
    IN icantidad integer,
    IN iprecio numeric(10,2),
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-12-17
-- Descripcion General: Editar registro a paquetes
*****************************************************************************/ 

DECLARE

--Variables de trabajo
id_nuevo_paquete integer;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al actualizar paquete ';

  --Se inserta el equipo
  UPDATE paquetes 
  SET nombre=inombre, medidas=imedidas, cantidad=icantidad, precio=iprecio 
  WHERE id=ipaquete;

  --Se genera log
  INSERT INTO logs_paquetes(id_paquete,usuario,descripcion,tipo) VALUES(ipaquete,iusuario,'Edito el paquete: ','U');

  flag       := true;
  mensaje    := 'Se actualizo el paquete correctamente';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER; 

  

CREATE OR REPLACE FUNCTION agregar_equipo(
    IN inombre character varying,
    IN itipo character varying,
    IN idescripcion character varying,
    IN ifecha_compra date,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-09-14
-- Descripcion General: Agregar registro a inventario_equipos
*****************************************************************************/ 

DECLARE

--Variables de trabajo
id_equipo_nuevo integer;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al guardar equipo ';

  --Se inserta el equipo
  INSERT INTO inventario_equipos(nombre,tipo,descripcion,fecha_compra) VALUES (inombre,itipo,idescripcion,ifecha_compra) RETURNING id INTO id_equipo_nuevo;

  --Se genera log
  INSERT INTO logs_equipos(id_equipo,usuario,descripcion,tipo) VALUES(id_equipo_nuevo,iusuario,'Creo el equipo: '||inombre,'C');

  flag       := true;
  mensaje    := 'Se guardo el equipo correctamente';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER; 



CREATE OR REPLACE FUNCTION public.editar_equipo(
    IN iequipo integer,
    IN inombre character varying,
    IN itipo character varying,
    IN idescripcion character varying,
    IN ifecha_compra date,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-09-14
-- Descripcion General: Editar registro a inventario_equipos
*****************************************************************************/ 

DECLARE

--Variables de trabajo
id_equipo_nuevo integer;
row_equipo RECORD;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al editar equipo ';

  SELECT nombre,tipo,descripcion,fecha_compra INTO row_equipo FROM inventario_equipos WHERE id=iequipo;
  
  --Se inserta el equipo
  UPDATE inventario_equipos 
  SET nombre=inombre, tipo=itipo, descripcion=idescripcion, fecha_compra=ifecha_compra 
  WHERE id=iequipo;

  --Se genera log
  INSERT INTO logs_equipos(id_equipo,usuario,descripcion,tipo) VALUES(iequipo,iusuario,'Actualizo el equipo: '||row_equipo.nombre||' (ID: '||iequipo||')','U');

  flag       := true;
  mensaje    := 'Se edito el equipo correctamente';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;





CREATE OR REPLACE FUNCTION agregar_reparacion(
    IN iequipo integer,
    IN iusuario integer,
    IN iproveedor integer,
    IN idescripcion_falla character varying,
    IN idescripcion_reparacion character varying,
    IN itotal numeric(10,2),
    IN ifecha date,
    IN iflag boolean,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-09-18
-- Descripcion General: Agregar registro a reparaciones_equipos
*****************************************************************************/ 

DECLARE

--Variables de trabajo
id_reparacion_nueva integer;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al guardar reparacion ';

 

  IF iflag = true THEN
  INSERT INTO reparaciones_equipos(id_equipo,id_usuario,id_proveedor,descripcion_falla,id_usuario_rep,descripcion_reparacion,total,fecha,flag)
  VALUES(iequipo,iusuario,iproveedor,idescripcion_falla,iusuario,idescripcion_reparacion,itotal,ifecha,iflag) RETURNING id INTO id_reparacion_nueva;

  INSERT INTO logs_equipos(id_equipo,usuario,descripcion,tipo) VALUES(iequipo,iusuario,'Registro la reparación #'||id_reparacion_nueva,'R');
  INSERT INTO logs_equipos(id_equipo,usuario,descripcion,tipo) VALUES(iequipo,iusuario,'Registro la confirmación de la reparación #'||id_reparacion_nueva,'D');
  ELSE
  INSERT INTO reparaciones_equipos(id_equipo,id_usuario,id_proveedor,descripcion_falla)
  VALUES(iequipo,iusuario,iproveedor,idescripcion_falla) RETURNING id INTO id_reparacion_nueva;

  INSERT INTO logs_equipos(id_equipo,usuario,descripcion,tipo) VALUES(iequipo,iusuario,'Registro la reparación #'||id_reparacion_nueva,'R');
  END IF;
   

  flag       := true;
  mensaje    := 'Se guardo la reparacion correctamente';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER; 


  
  CREATE OR REPLACE FUNCTION finalizar_reparacion(
    IN ireparacion integer,
    IN iequipo integer,
    IN iusuario integer,
    IN idescripcion_reparacion character varying,
    IN itotal numeric(10,2),
    IN ifecha date,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-09-19
-- Descripcion General: Finalizar registro a reparaciones_equipos
*****************************************************************************/ 

DECLARE

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al guardar reparacion ';

  UPDATE reparaciones_equipos 
  SET id_usuario_rep = iusuario, descripcion_reparacion = idescripcion_reparacion, total= itotal, fecha=ifecha, flag=true
  WHERE id=ireparacion;

  INSERT INTO logs_equipos(id_equipo,usuario,descripcion,tipo) VALUES(iequipo,iusuario,'Registro la confirmación de la reparación #'||ireparacion,'D');
   
  flag       := true;
  mensaje    := 'Se guardo la reparacion correctamente';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER; 


CREATE OR REPLACE FUNCTION agregar_servicio(
    IN iequipo integer,
    IN iusuario integer,
    IN iproveedor integer,
    IN idescripcion_servicio character varying,
    IN itotal numeric(10,2),
    IN ifecha date,
    IN iflag boolean,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-09-21
-- Descripcion General: Agregar registro a servicios_equipos
*****************************************************************************/ 

DECLARE

--Variables de trabajo
id_servicio_nuevo integer;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al guardar servicio ';

 
  IF iflag = true THEN

  INSERT INTO servicios_equipos(id_equipo,id_usuario,id_proveedor,descripcion_servicio,id_usuario_serv,total,fecha,flag)
  VALUES(iequipo,iusuario,iproveedor,idescripcion_servicio,iusuario,itotal,ifecha,iflag) RETURNING id INTO id_servicio_nuevo;

  INSERT INTO logs_equipos(id_equipo,usuario,descripcion,tipo) VALUES(iequipo,iusuario,'Registro del servicio #'||id_servicio_nuevo,'S');
  INSERT INTO logs_equipos(id_equipo,usuario,descripcion,tipo) VALUES(iequipo,iusuario,'Registro la confirmación del servicio #'||id_servicio_nuevo,'V');
  ELSE

  INSERT INTO servicios_equipos(id_equipo,id_usuario,id_proveedor,descripcion_servicio,fecha)
  VALUES(iequipo,iusuario,iproveedor,idescripcion_servicio,ifecha) RETURNING id INTO id_servicio_nuevo;

  INSERT INTO logs_equipos(id_equipo,usuario,descripcion,tipo) VALUES(iequipo,iusuario,'Registro del servicio #'||id_servicio_nuevo,'S');
  END IF;
   

  flag       := true;
  mensaje    := 'Se guardo el servicio correctamente';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER; 





  CREATE OR REPLACE FUNCTION eliminar_equipo(
    IN iequipo integer,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-09-24
-- Descripcion General: Eliminar registro a inventario_equipos
*****************************************************************************/ 

DECLARE

--Variables de trabajo
row_equipo RECORD;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al eliminar equipo ';

  SELECT nombre,tipo,descripcion,fecha_compra INTO row_equipo FROM inventario_equipos WHERE id=iequipo;

  --Se inserta el equipo
  UPDATE inventario_equipos 
  SET flag=false
  WHERE id=iequipo;

  --Se genera log
  INSERT INTO logs_equipos(id_equipo,usuario,descripcion,tipo) VALUES(iequipo,iusuario,'Elimino el equipo: '||row_equipo.nombre||' (ID: '||iequipo||')','D');

  flag       := true;
  mensaje    := 'Se elimino el equipo correctamente';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;



 /** -------------------- FIN EQUIPOS  -------------------- **/


  /** -------------------- CLIENTES  -------------------- **/



CREATE OR REPLACE FUNCTION crear_cliente_default(
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-10-01
-- Descripcion General: Agregar registro a tabla clientes
*****************************************************************************/ 

DECLARE

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al crear cliente ';

  IF (SELECT count(*) FROM clientes WHERE id=1) = 0 THEN
  INSERT INTO clientes(rfc,nombre,apellido_paterno,apellido_materno,nombre_comercial,email,telefono,celular,calle,
  num_ext,num_int,colonia,cod_postal,localidad,municipio,estado,estatus)
  VALUES ('XAXX010101000','PUBLICO EN GENERAL',' ',' ','PUBLICO EN GENERAL','DEFAULT@ALFA.COM','0','0','CONOCIDO','0','0','CENTRO','80000','CULIACAN','CULIACAN','SINALOA','C');

  flag       := true;
  mensaje    := 'Se creo el cliente default';
  RETURN NEXT;

  ELSE
  flag       := false;
  mensaje    := 'Ya existe el cliente default';
  RETURN NEXT;
  END IF;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER; 



CREATE OR REPLACE FUNCTION crear_cliente(
  IN irfc character varying,
  IN inombre character varying,
  IN iapellido_paterno character varying,
  IN iapellido_materno character varying,
  IN inombre_comercial character varying,
  IN iemail character varying,
  IN itelefono character varying,
  IN icelular character varying,
  IN icalle character varying,
  IN inum_ext character varying,
  IN inum_int character varying,
  IN icolonia character varying,
  IN ientre_calles character varying,
  IN icod_postal character varying,
  IN ilocalidad character varying,
  IN imunicipio character varying,
  IN iestado character varying,
  IN icredito numeric,
  IN iestatus character(1),
  IN iusuario integer,
  OUT id_cliente integer,
  OUT flag boolean,
  OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-10-01
-- Descripcion General: Agregar registro a tabla clientes
*****************************************************************************/ 

DECLARE
  --Variables de trabajo
  id_cliente_nuevo integer;
  

BEGIN
  --Variables de salida
  id_cliente := 0;
  flag       := false;
  mensaje    := 'Error al crear cliente ';

  INSERT INTO clientes(rfc,nombre,apellido_paterno,apellido_materno,nombre_comercial,email,telefono,celular,calle,num_ext,num_int,colonia,
  entre_calles,cod_postal,localidad,municipio,estado,credito,estatus)
  VALUES(irfc,inombre,iapellido_paterno,iapellido_materno,inombre_comercial,iemail,itelefono,icelular,icalle,inum_ext,inum_int,icolonia,
  ientre_calles,icod_postal,ilocalidad,imunicipio,iestado,icredito,iestatus) RETURNING id INTO id_cliente_nuevo;

  INSERT INTO logs_clientes(usuario,id_cliente,descripcion,tipo) VALUES(iusuario,id_cliente_nuevo,'Creo el cliente #'||id_cliente_nuevo||' - '||inombre||' '||iapellido_paterno||' '||COALESCE(iapellido_materno,'')||' ('||inombre_comercial||')','C');
  

  id_cliente := id_cliente_nuevo;
  flag       := true;
  mensaje    := 'Se creo correctamente el cliente ';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  id_cliente := 0;
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER; 


CREATE OR REPLACE FUNCTION editar_cliente(
  IN icliente integer,
  IN irfc character varying,
  IN inombre character varying,
  IN iapellido_paterno character varying,
  IN iapellido_materno character varying,
  IN inombre_comercial character varying,
  IN iemail character varying,
  IN itelefono character varying,
  IN icelular character varying,
  IN icalle character varying,
  IN inum_ext character varying,
  IN inum_int character varying,
  IN icolonia character varying,
  IN ientre_calles character varying,
  IN icod_postal character varying,
  IN ilocalidad character varying,
  IN imunicipio character varying,
  IN iestado character varying,
  IN icredito numeric,
  IN iestatus character(1),
  IN iusuario integer,
  OUT flag boolean,
  OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-10-01
-- Descripcion General: editar registro a tabla clientes
*****************************************************************************/ 

DECLARE

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al actualizar cliente ';

  UPDATE clientes SET rfc=irfc, nombre=inombre, apellido_paterno=iapellido_paterno, apellido_materno=iapellido_materno, nombre_comercial=inombre_comercial, email=iemail,
  telefono=itelefono, celular=icelular, calle=icalle, num_ext=inum_ext, num_int=inum_int, colonia=icolonia,
  entre_calles=ientre_calles, cod_postal=icod_postal, localidad=ilocalidad, municipio=imunicipio, estado=iestado, credito=icredito, estatus=iestatus
  WHERE id=icliente;

  INSERT INTO logs_clientes(usuario,id_cliente,descripcion,tipo) VALUES(iusuario,icliente,'Edito el cliente #'||icliente||' - '||inombre||' '||iapellido_paterno||' '||COALESCE(iapellido_materno,'')||' ('||inombre_comercial||')','U');

  flag       := true;
  mensaje    := 'Se actualizo correctamente el cliente ';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER; 


-- DROP FUNCTION public.eliminar_cliente(integer, integer);

CREATE OR REPLACE FUNCTION public.eliminar_cliente(
    IN icliente integer,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-10-01
-- Descripcion General: eliminar registro a tabla clientes
*****************************************************************************/ 

DECLARE
row_cliente RECORD;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al eliminar cliente ';

  SELECT nombre||' '||apellido_paterno||' '||apellido_materno AS nombre INTO row_cliente FROM clientes WHERE id=icliente;

  UPDATE clientes SET flag=false
  WHERE id=icliente;

  INSERT INTO logs_clientes(usuario,id_cliente,descripcion,tipo) VALUES(iusuario,icliente,'Elimino el cliente #'||icliente||' - '||row_cliente.nombre,'D');

  flag       := true;
  mensaje    := 'Elimino correctamente el cliente ';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;



  /** -------------------- FIN CLIENTES  -------------------- **/





