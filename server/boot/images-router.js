var express = require('express');

module.exports = function(server) {
  server.use('/recipeImages', express.static('cdn/recipeImages/'));
};