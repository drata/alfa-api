module.exports = function(app) {
  var Provider = app.models.provider;
  
  var providerData = {
    rfc: 'XAXX010101000',
    nombre: 'GASTOS SUCURSAL',
    nombre_comercial: 'GASTOS SUCURSAL',
    email: 'DEFAULT@KICHTO.COM',
    telefono: '0',
    celular: '0',
    calle: 'CONOCIDO',
    num_ext: '0',
    colonia: 'CENTRO',
    cod_postal: '8000',
    localidad: 'CULIACAN',
    municipio: 'CULIACAN',
    estado: 'SINALOA'
  };

  var createCommonProvider = function () {
    console.log('No existe el proveedor inicial, a continuación ' +
                'se procederá a crearlo...');

    Provider.create(providerData, function(err, result) {
      if(err) throw err;
      else console.log('...proveedor creado correctamente!');
    });
  };

  Provider.findOne({
    where: { id: 1 }
  }, function (err, provider) {
    if(err) throw err;
    if(!provider) createCommonProvider();
  });
};
