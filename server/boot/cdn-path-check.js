var fs = require('fs');
var path = require('path');

module.exports = function(server) {
  
  var cdnRoot           = path.resolve(__dirname, '../../cdn');
  var recipeImagesPath = path.resolve(__dirname, '../../cdn/recipeImages');

  if(!fs.existsSync(cdnRoot)) fs.mkdirSync(cdnRoot);
  if(!fs.existsSync(recipeImagesPath)) fs.mkdirSync(recipeImagesPath);
};
