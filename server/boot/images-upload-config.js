var multer = require('multer');

var availablePaths = {
  'cdn/recipeImages/': /\/api\/recipes\/images\/upload/
};

var getPathByRequest = function(req) {
  for(var path in availablePaths) {
    if(availablePaths[path].test(req.originalUrl)) return path;
  }
  return null;
};

var storageConfig = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, getPathByRequest(req));
  },

  filename: function (req, file, cb) {
    var fileExtension = file.originalname.split('.')[1];
    var newFileName = Date.now() + '.' + fileExtension;
    cb(null, newFileName);
  }
});

module.exports = function(app) {
  var upload = multer({
    storage: storageConfig
  });

  app.use(upload.single('file'));
};