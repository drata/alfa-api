module.exports = function(app) {
  var User = app.models.user;
  var adminData = {
    name: 'Creador',
    last_name: ' ',
    username: 'admin',
    email: 'default@grupoalsedav.com',
    password: 'As123456',
    profile: 'admin'
  };

  var createDefaultAdmin = function (argument) {
    console.log('No existe ningun administrador registrado, a continuación ' +
                'se procederá a crear uno con información por defecto...');

    User.create(adminData, function(err, user) {
      if(err) throw err;
      else console.log('...administrador creado correctamente!');
    });
  };

  User.findOne({
    where: { profile: 'admin' }
  }, function (err, user) {
    if(err) throw err;
    if(!user) createDefaultAdmin();
  });
};