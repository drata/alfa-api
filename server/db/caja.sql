CREATE OR REPLACE FUNCTION eliminar_tipo_pago(
    IN itipo integer,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-12-27
-- Descripcion General: Eliminar registro a cat_tipos_pagos
*****************************************************************************/ 

DECLARE

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al eliminar tipo de pago';

  UPDATE cat_tipos_pagos
  SET flag=false
  WHERE id=itipo;

  INSERT INTO logs_tipos_pagos(id_tipo,id_usuario,descripcion,tipo)
  VALUES(itipo,iusuario,'Elimino el tipo de pago' ,'D');

  flag       := true;
  mensaje    := 'Se elimino correctamente el tipo de pago';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;

CREATE OR REPLACE FUNCTION agregar_tipo_pago(
    IN inombre character varying,
    IN imovimiento character(1),
    IN ivisible boolean,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-12-27
-- Descripcion General: Agregar registro a cat_tipos_pagos
*****************************************************************************/ 

DECLARE
id_tipo_nuevo integer;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al agregar tipo de pago';

  INSERT INTO cat_tipos_pagos(nombre,visible,movimiento)
  VALUES (inombre,ivisible,imovimiento)
  RETURNING id INTO id_tipo_nuevo;

  INSERT INTO logs_tipos_pagos(id_tipo,id_usuario,descripcion,tipo)
  VALUES(id_tipo_nuevo,iusuario,'Agrego el tipo de pago: '||inombre,'C');

  flag       := true;
  mensaje    := 'Se agrego correctamente el tipo de pago';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;


CREATE OR REPLACE FUNCTION editar_tipo_pago(
    IN itipo integer,
    IN inombre character varying,
    IN imovimiento character(1),
    IN ivisible boolean,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-12-27
-- Descripcion General: Editar registro a cat_tipos_pagos
*****************************************************************************/ 

DECLARE

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al actualizar tipo de pago';

  UPDATE cat_tipos_pagos
  SET nombre=inombre, visible=ivisible, movimiento=imovimiento
  WHERE id=itipo;

  INSERT INTO logs_tipos_pagos(id_tipo,id_usuario,descripcion,tipo)
  VALUES(itipo,iusuario,'Edito el tipo de pago','U');

  flag       := true;
  mensaje    := 'Se actualizo correctamente el tipo de pago';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;

CREATE OR REPLACE FUNCTION cerrar_corte(
    IN icorte integer,
    IN isaldo_real numeric(10,2),
    IN icomentarios character varying,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-12-27
-- Descripcion General: Cerar corte
*****************************************************************************/ 

DECLARE

--Variables de trabajo
comentarios_dif character varying;
diferencia_corte numeric(10,2);
dif_corte_nuevo numeric(10,2);
tipo_mov character(1);
saldo_caja numeric(10,2);
saldo_final_corte numeric(10,2);
corte_actual RECORD;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al generar ciere de corte';

  --Obtener datos del corte actual
  SELECT saldo_apertura,estatus,fecha INTO corte_actual FROM cortes WHERE id=icorte;

  --Verificar que el corte no tenga un estatus CERRADO
  IF corte_actual.estatus = 'C' THEN
    flag       := true;
    mensaje    := 'El corte seleccionado ya fue cerrado';
    RETURN NEXT;
    RETURN;
  END IF;

  --Obtener el saldo actual del corte
  SELECT tsaldo_caja INTO saldo_caja FROM obtener_montos_corte(icorte);

  diferencia_corte := isaldo_real - saldo_caja;

  IF diferencia_corte = 0 THEN
    saldo_final_corte:= isaldo_real;

  ELSEIF diferencia_corte > 0 THEN

    INSERT INTO movimientos(id_corte,id_proveedor,id_usuario,documento,concepto,importe,metodo,tipo,movimiento,estatus,fecha)
    VALUES(icorte,0,iusuario,0,'ABONO POR SOBRANTE EN CIERRE',diferencia_corte,1,3,'A','R',corte_actual.fecha);
    
    saldo_final_corte:= (SELECT tsaldo_caja FROM obtener_montos_corte(icorte));

    INSERT INTO incidencias_cierre_corte(id_corte,id_usuario,motivo,movimiento,importe) 
    VALUES(icorte,iusuario,icomentarios,'A',diferencia_corte);

  ELSEIF diferencia_corte < 0 THEN
    
    dif_corte_nuevo:= diferencia_corte * -1;
    INSERT INTO movimientos(id_corte,id_proveedor,id_usuario,documento,concepto,importe,metodo,tipo,movimiento,estatus,fecha)
    VALUES(icorte,0,iusuario,0,'CARGO POR FALTANTE EN CIERRE',dif_corte_nuevo,1,2,'C','R',corte_actual.fecha);
    
    saldo_final_corte:= (SELECT tsaldo_caja FROM obtener_montos_corte(icorte));

    INSERT INTO incidencias_cierre_corte(id_corte,id_usuario,motivo,movimiento,importe) 
    VALUES(icorte,iusuario,icomentarios,'C',dif_corte_nuevo);

  ELSE 
    flag       := false;
    mensaje    := 'Error al generar ciere de corte';
    RETURN NEXT;
    RETURN;
  END IF;

  --Se genera el cierre
  UPDATE cortes 
  SET estatus='C', 
    saldo_cierre=isaldo_real, 
    diferencia=diferencia_corte, 
    saldo_final=saldo_final_corte 
  WHERE id=icorte;

  --Insertar log
  INSERT INTO logs_cortes(id_corte,id_usuario,descripcion,tipo)
  VALUES (icorte,iusuario,'Realizo el cierre del corte ID: '||icorte,'F');

  flag       := true;
  mensaje    := 'Se realizo correctamente el cierre';
  RETURN NEXT;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
    flag       := false;
    mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER; 


CREATE OR REPLACE FUNCTION public.obtener_montos_corte(
    IN icorte integer,
    OUT tsaldo_caja numeric,
    OUT tabonos_caja numeric,
    OUT tcargos_caja numeric,
    OUT tcargos numeric,
    OUT tabono numeric,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-12-27
-- Descripcion General: Obtener montos del corte
*****************************************************************************/ 

DECLARE
saldo_corte numeric;

BEGIN

  --Inicializar variables de salida
  tabonos_caja := (SELECT COALESCE(sum(importe),0) FROM movimientos WHERE movimiento='A' AND metodo=1 AND id_corte=icorte);
  tcargos_caja := (SELECT COALESCE(sum(importe),0) FROM movimientos WHERE movimiento='C' AND metodo=1 AND id_corte=icorte);
  tabono       := (SELECT COALESCE(sum(importe),0) FROM movimientos WHERE movimiento='A' AND id_corte=icorte);
  tcargos      := (SELECT COALESCE(sum(importe),0) FROM movimientos WHERE movimiento='C' AND id_corte=icorte);
  
  IF (SELECT estatus FROM cortes WHERE id=icorte) = 'A' THEN
    saldo_corte := (((SELECT saldo_apertura FROM cortes WHERE id=icorte) + tabonos_caja ) - tcargos_caja);
  ELSE
    saldo_corte := (SELECT saldo_final FROM cortes WHERE id=icorte);
  END IF;
  
  tsaldo_caja  := COALESCE(saldo_corte,0);
  flag       := true;
  mensaje      := 'Se obtuvo correctamente la informacion';  
  RETURN NEXT; 

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
    tsaldo_caja  := 0;
    tabonos_caja := 0;
    tcargos_caja := 0;
    tcargos      := 0;
    tabono       := 0;
    flag       := false;
    mensaje      := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;


CREATE OR REPLACE FUNCTION agregar_movimiento(
    IN idocumento integer,
    IN iproveedor integer,
    IN iconcepto character varying,
    IN iimporte numeric(10,2),
    IN imetodo integer,
    IN itipo integer,
    IN imovimiento character(1),
    IN iusuario integer,
    OUT id_movimiento integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-12-27
-- Descripcion General: Agregar registro a movimientos
*****************************************************************************/ 

DECLARE

--Variables de trabajo
id_movimiento_nuevo integer;
data_corte RECORD;
fecha_servidor date;
mov text;
  
BEGIN
  --Variables de salida
  id_movimiento := 0;
  flag          := false;
  mensaje       := 'Error al guardar movimiento ';

  SELECT (now() AT TIME ZONE 'America/Mazatlan')::date INTO fecha_servidor;
  SELECT z.fecha_corte,z.id_corte,z.estado,z.mensaje INTO data_corte FROM obtener_corte_actual() AS z;

  IF data_corte.estado = 'A' THEN
  
    --Se inserta movimiento
    INSERT INTO movimientos(id_corte,id_proveedor,id_usuario,documento,concepto,importe,metodo,tipo,movimiento,estatus,fecha)
    VALUES(data_corte.id_corte,iproveedor,iusuario,idocumento,iconcepto,iimporte,imetodo,itipo,imovimiento,'R',fecha_servidor)
    RETURNING id INTO id_movimiento_nuevo;

    IF imovimiento ='A' THEN
      mov:= 'abono';
    ELSE
      mov:= 'pago';
    END IF;

    --Se genera log
    INSERT INTO logs_movimientos(id_movimiento,id_usuario,descripcion,tipo)
    VALUES(id_movimiento_nuevo,iusuario,'Agrego un '||mov,'C');

    id_movimiento := id_movimiento_nuevo;
    flag          := true;
    mensaje       := 'Se guardo el movimiento correctamente';
    RETURN NEXT;
  ELSE
    id_movimiento := 0;
    flag          := false;
    mensaje       := 'Actualmente no hay un corte activo, es necesario aperturar caja para autorizar pago';
    RETURN NEXT;
  END IF;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  id_movimiento   := 0;
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER; 


  
CREATE OR REPLACE FUNCTION public.agregar_corte(
    IN ifecha date,
    IN isaldo_apertura numeric,
    IN icomentarios character varying,
    IN iusuario integer,
    OUT id_corte integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-12-03
-- Descripcion General: Agregar registro a cortes
*****************************************************************************/ 

DECLARE

--Variables de trabajo
id_corte_nuevo integer;
corte_actual RECORD;
diferencia_corte numeric(10,2);
saldo_cierre_actual numeric(10,2);
dif_corte_nuevo numeric(10,2);



BEGIN
  --Variables de salida
  id_corte   := 0;
  flag       := false;
  mensaje    := 'Error al guardar corte ';


  IF (SELECT count(*) FROM cortes WHERE fecha=ifecha) > 0 THEN
  mensaje    := 'Error, ya hay un corte del dia '||ifecha;
  RETURN NEXT;
  RETURN;
  END IF;

  --Obtener información corte actual
  SELECT z.id_corte,z.estado INTO corte_actual FROM obtener_corte_actual() AS z;

  --Verificar que el corte no tenga un estatus CERRADO
  IF corte_actual.estado = 'C' THEN

    --Se inserta el corte
    INSERT INTO cortes(id_usuario,saldo_apertura,fecha,estatus) 
    VALUES(iusuario,isaldo_apertura,ifecha,'A')
    RETURNING id INTO id_corte_nuevo;
  
    --Obtener el saldo actual del corte
    SELECT saldo_cierre INTO saldo_cierre_actual FROM cortes WHERE id=corte_actual.id_corte;
    
    diferencia_corte := isaldo_apertura - saldo_cierre_actual;

    --Generar cargo por diferencia en apertura
    IF diferencia_corte  > 0 THEN

      INSERT INTO incidencias_apertura_corte(id_corte,id_usuario,motivo,movimiento,importe) 
      VALUES(corte_actual.id_corte,iusuario,icomentarios,'A',diferencia_corte);


    ELSEIF diferencia_corte < 0 THEN

      dif_corte_nuevo:= diferencia_corte * -1;
      INSERT INTO incidencias_apertura_corte(id_corte,id_usuario,motivo,movimiento,importe) 
      VALUES(corte_actual.id_corte,iusuario,icomentarios,'C',dif_corte_nuevo);

    END IF;

  ELSE
    --Se inserta el corte
    INSERT INTO cortes(id_usuario,saldo_apertura,fecha,estatus) 
    VALUES(iusuario,isaldo_apertura,ifecha,'A')
    RETURNING id INTO id_corte_nuevo;
    
  END IF;

  --Insertar log
  INSERT INTO logs_cortes(id_corte,id_usuario,descripcion,tipo)
  VALUES (id_corte_nuevo,iusuario,'Creo el corte ID: '||id_corte_nuevo||' con un saldo de apertura de: $'||isaldo_apertura,'C');

  id_corte   := id_corte_nuevo;
  flag       := true;
  mensaje    := 'Se guardo el corte correctamente';
  RETURN NEXT;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  id_corte   := 0;
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;


CREATE OR REPLACE FUNCTION public.obtener_corte_actual(
    OUT fecha_corte text,
    OUT id_corte integer,
    OUT estado character,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-11-29
-- Descripcion General: Obtener fecha actual y saldo ultimo corte
*****************************************************************************/ 

DECLARE
fecha_servidor text;
fecha_corte_actual text;
id_corte_actual integer;

BEGIN

  SELECT (now() AT TIME ZONE 'America/Mazatlan')::date INTO fecha_servidor;

  --Verificar disponibilidad Caja
  IF (SELECT count(*) FROM cortes WHERE estatus = 'A') >= 1 THEN
    SELECT COALESCE(max(id),0)::integer INTO id_corte_actual FROM cortes WHERE estatus = 'A' LIMIT 1;
    SELECT fecha::text INTO fecha_corte_actual FROM cortes WHERE id = id_corte_actual;

    IF (fecha_corte_actual::date <  fecha_servidor::date) = true THEN
  fecha_corte  := fecha_corte_actual;
  id_corte     := id_corte_actual;
  estado       := 'D';
  mensaje      := 'Es necesario cerrar el corte actual para generar movimientos, ya que la fecha del corte no corresponde a la fecha actual';
  RETURN NEXT;
    ELSE 
  fecha_corte  := fecha_corte_actual;
  id_corte     := id_corte_actual;
  estado       := 'A';
  mensaje      := 'Actualmente hay un corte activo, es necesario cerrar el corte actual para proceder a crear uno nuevo';
  RETURN NEXT;
    END IF;
    
    
  ELSE
    --Obtener información del ultimo corte
    SELECT COALESCE(max(id),0)::integer INTO id_corte_actual FROM cortes WHERE estatus = 'C';
    SELECT fecha::text INTO fecha_corte_actual  FROM cortes WHERE id = id_corte_actual;
    IF id_corte_actual = 0 THEN
      fecha_corte  := fecha_servidor;
      id_corte     := id_corte_actual;
      estado       := 'N';
      mensaje      := 'Corte inicial';
      RETURN NEXT;
    ELSE
      fecha_corte  := fecha_corte_actual;
      id_corte     := id_corte_actual;
      estado       := 'C';
      mensaje      := 'Se obtuvo información del corte anterior';
      RETURN NEXT;
    END IF;
    
  END IF;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  fecha_corte := '';
  id_corte     := '';
  estado       := 'E';
  mensaje      := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;
