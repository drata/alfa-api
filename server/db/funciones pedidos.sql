
  
CREATE OR REPLACE FUNCTION generar_nota_de_venta_rapida(
    IN icliente integer,
    IN idatos_cliente jsonb,
    IN iproductos jsonb,
    IN imetodos_pago jsonb,
    IN iflag_factura boolean,
    IN iflag_medida boolean,
    IN iflag_montaje boolean,
    IN icomentarios character varying,
    IN itipo_vta character,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying,
    OUT id_nota integer)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-12-26
-- Descripcion General: Generar una nota de venta rapida
*****************************************************************************/ 

DECLARE
  --Variables de trabajo
  linea record;
  total_producto numeric(10,2);
  subtotal_nota numeric(10,2);
  iva_nota numeric(10,2);
  descuento_nota numeric(10,2);
  total_nota numeric(10,2);
  id_nuevo_pedido integer;
  id_nueva_nota integer;
  total_pagado numeric(10,2);
  saldo_nota numeric(10,2);
  abono RECORD;
  movimiento RECORD;
    
BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al generar nota de venta ';
  id_nota  := 0;  

  --Eliminar tablas temporales
  DROP TABLE IF EXISTS datos_cliente_temp;
  DROP TABLE IF EXISTS productos_temp;
  DROP TABLE IF EXISTS metodos_pago_temp;

  --Crear tablas temporales
  CREATE TABLE datos_cliente_temp(
    id integer,
    rfc character varying,
    nombre character varying,
    apellido_paterno character varying,
    apellido_materno character varying,
    email character varying,
    telefono character varying,
    celular character varying,
    calle character varying,
    num_ext character varying,
    num_int character varying,
    colonia character varying,
    entre_calles character varying,
    cod_postal character varying,
    localidad character varying,
    municipio character varying,
    estado character varying,
    credito numeric,
    estatus character(1)
  );  

  CREATE TABLE productos_temp(
    nombre character varying,
    medidas character varying,
    cantidad integer,
    precio_u numeric (10,2),
    total numeric (10,2),
    paquete boolean
  );

  CREATE TABLE metodos_pago_temp(
    id integer,
    importe numeric (10,2)
  );  

  RAISE NOTICE 'METODOS: %',imetodos_pago;
  --Obtener metodo de pago
  INSERT INTO metodos_pago_temp(id,importe)
  SELECT id,importe 
  FROM jsonb_to_recordset((imetodos_pago)) AS (id integer, importe numeric (10,2));

  --Obtener total pagado
  SELECT sum(importe) INTO total_pagado FROM metodos_pago_temp;
  RAISE NOTICE '%',total_pagado;

  --Obtener Datos Cliente
  INSERT INTO datos_cliente_temp 
  SELECT id,rfc,nombre,apellido_paterno,apellido_materno,email,telefono,celular,
    calle,num_ext,num_int,colonia,entre_calles,cod_postal,localidad,municipio,estado,credito,estatus
  FROM jsonb_to_record((idatos_cliente)) AS  (id integer,rfc character varying,nombre character varying,apellido_paterno character varying,
  apellido_materno character varying,email character varying,telefono character varying,
  celular character varying,calle character varying,num_ext character varying,num_int character varying,
  colonia character varying,entre_calles character varying,cod_postal character varying,
  localidad character varying,municipio character varying,estado character varying,
  credito numeric,estatus character(1));

  --Obtener Lista de productos
  FOR linea IN SELECT nombre,medidas,cantidad,precio,paquete FROM jsonb_to_recordset((iproductos))  
    AS  (nombre character varying, medidas character varying, cantidad integer, 
    precio numeric (10,2), paquete boolean ) LOOP

    total_producto := linea.cantidad * linea.precio;

    INSERT INTO productos_temp(nombre,medidas,cantidad,precio_u,total,paquete)
    VALUES (linea.nombre,linea.medidas,linea.cantidad,linea.precio,total_producto,linea.paquete);

  END LOOP;

  --Obtener subtotal de pedido
  SELECT sum(precio_u * cantidad) INTO subtotal_nota FROM productos_temp;

  --Sumar IVA en caso de ser factura
  IF iflag_factura = true THEN
    iva_nota := 0.16 * subtotal_nota;
  ELSE
    iva_nota := 0;
  END IF;

  --Obtener total de pedido
  descuento_nota := 0;
  total_nota = subtotal_nota + iva_nota;

   --Obtener Saldo
  saldo_nota := total_nota - total_pagado;

  --Crear pedido
  INSERT INTO pedidos(id_cliente,flag_factura,flag_medida,flag_montaje,comentarios,subtotal,descuento,iva,total,id_usuario)
  VALUES(icliente,iflag_factura,iflag_medida,iflag_montaje,icomentarios,subtotal_nota,descuento_nota,iva_nota,total_nota,iusuario) 
  RETURNING id INTO id_nuevo_pedido;

  --Anexar datos de contacto de pedido
  INSERT INTO pedido_contacto(id_pedido,id_cliente,rfc,nombre,apellido_paterno,
  apellido_materno,email,telefono,celular,calle,num_ext,num_int,colonia,
  entre_calles,cod_postal,localidad,municipio,estado)
  SELECT id_nuevo_pedido,id,rfc,nombre,apellido_paterno,apellido_materno,email,telefono,celular,
  calle,num_ext,num_int,colonia,entre_calles,cod_postal,localidad,municipio,estado  
  FROM datos_cliente_temp;

  --Anexar lista de productos de pedido
  INSERT INTO pedido_detalle(id_pedido,nombre,medidas,cantidad,precio_u,total,paquete)
  SELECT id_nuevo_pedido,nombre,medidas,cantidad,precio_u,total,paquete
  FROM productos_temp;

  --Crear Nota
  INSERT INTO notas(id_cotizacion,id_pedido,id_cliente,rfc,nombre_cliente,email,telefono,celular,
  calle,num_ext,num_int,colonia,entre_calles,cod_postal,localidad,municipio,estado,
  tipo_vta,subtotal,descuento,iva,total,saldo,flag_factura)
  SELECT 0,id_nuevo_pedido,id,rfc,COALESCE(nombre,'') || ' ' ||COALESCE(apellido_paterno,'') ||' '||COALESCE(apellido_materno,'') AS nombre_cliente,email,telefono,celular,
  calle,num_ext,num_int,colonia,entre_calles,cod_postal,localidad,municipio,estado,
  itipo_vta,subtotal_nota,descuento_nota,iva_nota,total_nota,saldo_nota,iflag_factura  
  FROM datos_cliente_temp
  RETURNING id INTO id_nueva_nota;

  --Anexar lista de productos a nota de venta, con el margen aplicado
  INSERT INTO nota_detalle(id_nota,nombre,medidas,cantidad,precio_u,total,paquete)
  SELECT id_nueva_nota,nombre,medidas,cantidad,precio_u,total,paquete
  FROM productos_temp;

  --Generar Movimiento de Abono
  FOR abono IN SELECT id,sum(importe) AS total FROM metodos_pago_temp GROUP BY id LOOP
  RAISE NOTICE 'Total Abono: %',abono.total;
    SELECT z.id_movimiento,z.flag,z.mensaje INTO movimiento FROM agregar_movimiento(id_nueva_nota,0,'ABONO NOTA DE VENTA',abono.total,abono.id,6,'A',iusuario) AS z;
    IF movimiento.flag = false THEN
      RAISE EXCEPTION '%',movimiento.mensaje;
    END IF;
  END LOOP;

  --Actualizando información del pedido
  UPDATE pedidos SET estatus = 'V' WHERE id = id_nuevo_pedido;

  --Eliminar tablas temporales
  DROP TABLE productos_temp;
  DROP TABLE datos_cliente_temp;
  DROP TABLE metodos_pago_temp;

  flag       := true;
  mensaje    := 'Se genero la nota de venta correctamente';
  id_nota    := id_nueva_nota;
  RETURN NEXT;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  id_nota    := 0;
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;


CREATE OR REPLACE FUNCTION public.generar_abono_a_nota_de_venta(
    IN inota integer,
    IN imetodos_pago jsonb,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2019-01-15
-- Descripcion General: Generar abono a notas de venta
*****************************************************************************/ 

DECLARE
  --Variables de trabajo
  abono RECORD;
  total_pagado numeric(10,2);
  saldo_nota numeric(10,2);
  saldo_restante numeric(10,2);
  movimiento RECORD;
BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al generar abono a nota de venta ';

  --Eliminar tablas temporales
  DROP TABLE IF EXISTS metodos_pago_temp;
  
  CREATE TABLE metodos_pago_temp(
    id integer,
    importe numeric (10,2)
  );  
  
  --Obtener metodo de pago
  INSERT INTO metodos_pago_temp(id,importe)
  SELECT id,importe 
  FROM jsonb_to_recordset((imetodos_pago)) AS (id integer, importe numeric (10,2));

  --Obtener total pagado
  SELECT sum(importe) INTO total_pagado FROM metodos_pago_temp;
  
  --Obtener Saldo
  SELECT saldo INTO saldo_nota FROM notas WHERE id=inota;

  --Obtener Saldo Restante
  saldo_restante := saldo_nota - total_pagado;

  --Generar Movimiento de Abono
  FOR abono IN SELECT id,sum(importe) AS total FROM metodos_pago_temp GROUP BY id LOOP
    SELECT z.id_movimiento,z.flag,z.mensaje INTO movimiento FROM agregar_movimiento(inota,0,'ABONO NOTA DE VENTA',abono.total,abono.id,6,'A',iusuario) AS z;
    IF movimiento.flag = false THEN
      RAISE EXCEPTION '%',movimiento.mensaje;
    END IF;
  END LOOP;

  --Actualizar Saldo Nota
  UPDATE notas SET saldo = saldo_restante WHERE id=inota;

  --Eliminar tablas temporales
  DROP TABLE metodos_pago_temp;

  flag       := true;
  mensaje    := 'Se genero el abono a nota de venta correctamente';
  RETURN NEXT;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := SQLERRM;  
  RETURN NEXT; 
END
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;

CREATE OR REPLACE FUNCTION public.generar_nota_de_venta(
    IN icotizacion integer,
    IN iflag_factura boolean,
    IN itipo_vta character,
    IN imetodos_pago jsonb,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying,
    OUT id_nota integer)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-11-21
-- Descripcion General: Generar una nota de venta, apartir de una  cotización
*****************************************************************************/ 

DECLARE
  --Variables de trabajo
  row_cotizacion RECORD;
  row_cliente RECORD;
  precio_producto numeric(10,2);
  total_producto numeric(10,2);
  subtotal_producto numeric(10,2);
  margen_producto numeric(10,2);
  total_neto_nota numeric(10,2);
  total_margen_nota numeric(10,2);
  subtotal_nota numeric(10,2);
  descuento_nota numeric(10,2);
  iva_nota numeric(10,2);
  total_nota numeric(10,2);
  id_nueva_nota integer;
  linea RECORD;
  total_pagado numeric(10,2);
  saldo_nota numeric(10,2);
  total_neto  numeric(10,2);
  total_margen  numeric(10,2);
  abono RECORD;
  movimiento RECORD;
BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al generar nota de venta ';
  id_nota    := 0;   

  --Eliminar tablas temporales
  DROP TABLE IF EXISTS productos_temp;
  DROP TABLE IF EXISTS metodos_pago_temp;

  --Crear tabla de productos temporales
  CREATE TABLE productos_temp(
    id serial PRIMARY KEY NOT NULL,
    nombre character varying,
    medidas character varying,
    cantidad integer,
    precio_u numeric (10,2),
    total numeric (10,2),
    paquete boolean
  );

  
  CREATE TABLE metodos_pago_temp(
    id integer,
    importe numeric (10,2)
  );  
  
  --Obtener metodo de pago
  INSERT INTO metodos_pago_temp(id,importe)
  SELECT id,importe 
  FROM jsonb_to_recordset((imetodos_pago)) AS (id integer, importe numeric (10,2));

  --Obtener total pagado
  SELECT sum(importe) INTO total_pagado FROM metodos_pago_temp;
  
  --Obtener información Cotización
  SELECT id_pedido INTO row_cotizacion FROM cotizaciones WHERE id=icotizacion;

  --Obtener datos Cliente
  SELECT id_cliente,rfc,
    COALESCE(nombre,'') || ' ' ||COALESCE(apellido_paterno,'') ||' '||COALESCE(apellido_materno,'') AS nombre_cliente,
    COALESCE(email,'SIN CORREO') AS email, telefono, celular,calle,num_ext,num_int,colonia,entre_calles,cod_postal,
    localidad,municipio,estado
  INTO row_cliente
  FROM pedido_contacto
  WHERE id_pedido = row_cotizacion.id_pedido;

  --Obtener Lista de productos Cotización
  FOR linea IN SELECT d.nombre,d.medidas,d.cantidad,d.tipo_precio,d.margen,d.precio_u,d.paquete FROM cotizacion_detalle as d 
         WHERE d.id_cotizacion=icotizacion AND d.flag=true LOOP

    precio_producto := linea.precio_u + (linea.precio_u*(linea.margen/100));
    total_producto  := linea.cantidad * precio_producto;

    INSERT INTO productos_temp(nombre,medidas,cantidad,precio_u,total,paquete)
    VALUES (linea.nombre,linea.medidas,linea.cantidad,precio_producto,total_producto,linea.paquete);

  END LOOP;

  --Calcular el TOTAL NETO
  SELECT sum(total) INTO subtotal_nota FROM productos_temp;
  
  --Sumar IVA en caso de ser factura
  IF iflag_factura = true THEN
    iva_nota := 0.16 * subtotal_nota;
  ELSE
    iva_nota := 0;
  END IF;

  --Obtener total de pedido
  descuento_nota := 0;
  total_nota = subtotal_nota + iva_nota;
  

  --Obtener Saldo
  saldo_nota = total_nota - total_pagado;

  --Crear Nota
  INSERT INTO notas(id_cotizacion,id_pedido,id_cliente,rfc,nombre_cliente,email,telefono,celular,
        calle,num_ext,num_int,colonia,entre_calles,cod_postal,localidad,municipio,estado,
        tipo_vta,subtotal,descuento,iva,total,saldo,flag_factura)
  VALUES (icotizacion,row_cotizacion.id_pedido,row_cliente.id_cliente,row_cliente.rfc,row_cliente.nombre_cliente,
    row_cliente.email,row_cliente.telefono,row_cliente.celular,row_cliente.calle,row_cliente.num_ext,
    row_cliente.num_int,row_cliente.colonia,row_cliente.entre_calles,row_cliente.cod_postal,row_cliente.localidad,
    row_cliente.municipio,row_cliente.estado,itipo_vta,subtotal_nota,descuento_nota,iva_nota,total_nota,saldo_nota,iflag_factura) 
    RETURNING id INTO id_nueva_nota;

  --Anexar lista de productos a nota de venta, con el margen aplicado
  INSERT INTO nota_detalle(id_nota,nombre,medidas,cantidad,precio_u,total,paquete)
  SELECT  id_nueva_nota,nombre,medidas,cantidad,precio_u,total,paquete
  FROM productos_temp;

    --SELECT id,sum(importe) FROM metodos_pago_temp GROUP BY id;
  SELECT sum(importe) INTO total_pagado FROM metodos_pago_temp;
  ---RAISE NOTICE 'Total Pagado: %',total_pagado;

  --Generar Movimiento de Abono
  FOR abono IN SELECT id,sum(importe) AS total FROM metodos_pago_temp GROUP BY id LOOP
  RAISE NOTICE 'Total Abono: %',abono.total;
  SELECT z.id_movimiento,z.flag,z.mensaje INTO movimiento FROM agregar_movimiento(id_nueva_nota,0,'ABONO NOTA DE VENTA',abono.total,abono.id,6,'A',iusuario) AS z;
  IF movimiento.flag = false THEN
    RAISE EXCEPTION '%',movimiento.mensaje;
  END IF;
  END LOOP;

  --Actualizando información del pedido
  UPDATE pedidos SET cotizacion = icotizacion, estatus = 'V' WHERE id = row_cotizacion.id_pedido;

  --Finalizar cotizaciones del pedido
  UPDATE cotizaciones SET estatus = 'C' WHERE id_pedido = row_cotizacion.id_pedido AND id <> icotizacion;

  --Actualizando estatus cotizacion
  UPDATE cotizaciones SET estatus = 'V', flag_factura=iflag_factura WHERE id = icotizacion;

  --Eliminar tablas temporales
  DROP TABLE productos_temp;
  DROP TABLE metodos_pago_temp;

  flag       := true;
  mensaje    := 'Se genero la nota de venta correctamente';
  id_nota    := id_nueva_nota;
  RETURN NEXT;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := SQLERRM;  
  id_nota    := 0;
  RETURN NEXT; 
END
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;


CREATE OR REPLACE FUNCTION agregar_respuesta_cotizacion(
    IN icotizacion integer,
    IN itipo character,
    IN icontacto character varying,
    IN imedio character varying,
    IN icomentarios character varying,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-11-12
-- Descripcion General: Agregar respuesta a la cotización
*****************************************************************************/ 

DECLARE
row_cotizacion RECORD;
num_productos integer;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al agregar respuesta a la cotización ';

  SELECT fecha_entrega,id_usuario,asignacion INTO row_cotizacion FROM cotizaciones WHERE id = icotizacion;

  IF (itipo IN ('P','R')) THEN
    INSERT INTO cotizacion_repuestas(id_cotizacion,comentarios,contacto,medio,tipo) 
    VALUES(icotizacion,icomentarios,icontacto,imedio,itipo);

    UPDATE cotizaciones SET estatus = itipo WHERE id = icotizacion;

    IF itipo = 'R' THEN
      PERFORM generar_log_cotizacion(icotizacion,'Rechazada por Cliente','R',iusuario);
    END IF;

    flag       := true;
    mensaje    := 'Se agrego correctamente la respuesta a la cotización';
    RETURN NEXT;
  ELSE 
    IF row_cotizacion.fecha_entrega IS NULL THEN
      flag       := false;
      mensaje    := 'Error al guardar, es necesario agregar fecha de entrega a la cotización';
      RETURN NEXT;
    ELSE
      IF (SELECT count(id) FROM cotizacion_detalle WHERE id_cotizacion = icotizacion) > 0 THEN
      
        INSERT INTO cotizacion_repuestas(id_cotizacion,comentarios,contacto,medio,tipo) 
        VALUES(icotizacion,icomentarios,icontacto,imedio,itipo);

        UPDATE cotizaciones SET estatus = itipo WHERE id = icotizacion;

        PERFORM generar_log_cotizacion(icotizacion,'Aceptada por Cliente','A',iusuario);

        flag       := true;
        mensaje    := 'Se agrego correctamente la respuesta a la cotización';
        RETURN NEXT;
      ELSE 
        flag       := false;
        mensaje    := 'Error al guardar, es necesario agregar productos a la cotización';
        RETURN NEXT;

      END IF;

    END IF;

  END IF;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;


CREATE OR REPLACE FUNCTION public.generar_log_cotizacion(
    IN icotizacion integer,
    IN idescripcion character varying,
    IN itipo character(1),
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-11-06
-- Descripcion General: Generar log cotización
*****************************************************************************/ 

DECLARE


BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al generar log';

  --Generar Log
  INSERT INTO logs_cotizacion(id_cotizacion,id_usuario,descripcion,tipo)
  VALUES(icotizacion,iusuario,idescripcion,itipo);

  flag       := true;
  mensaje    := 'Se genero el log correctamente';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;

CREATE OR REPLACE FUNCTION public.guardar_productos_cotizacion(
    IN icotizacion integer,
    IN iflag_factura boolean,
    IN iproductos jsonb,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-11-07
-- Descripcion General: Guardar productos y  variables de cotización 
*****************************************************************************/ 

DECLARE
linea record;
total_producto numeric(10,2);
total_neto  numeric(10,2);
total_margen  numeric(10,2);
subtotal_pedido numeric(10,2);
iva_pedido numeric(10,2);
descuento_pedido numeric(10,2);
total_pedido numeric(10,2);
cambio_servicio boolean;

BEGIN

  --Variables de salida
  flag       := false;
  mensaje    := 'Error al actualizar productos y variables de cotizacion ';

  --Eliminar tablas temporales
  DROP TABLE IF EXISTS productos_temp;

  CREATE TABLE productos_temp(
    id integer PRIMARY KEY NOT NULL,
    nombre character varying,
    medidas character varying,
    cantidad integer,
    precio_u numeric (10,2),
    total numeric (10,2),
    paquete boolean,
    margen integer
  );
  
  --Obtener Lista de productos
  FOR linea IN SELECT id,nombre,medidas,cantidad,precio_u,paquete,tipo_precio,margen FROM jsonb_to_recordset((iproductos))  
    AS  (id integer,nombre character varying, medidas character varying, cantidad integer, 
    precio_u numeric (10,2), paquete boolean, tipo_precio integer, margen integer ) LOOP

    total_neto := linea.cantidad * linea.precio_u;
    total_margen := total_neto * (linea.margen / 100);
    total_producto := total_neto + total_margen;

    UPDATE cotizacion_detalle 
    SET nombre = linea.nombre, medidas = linea.medidas, cantidad = linea.cantidad, precio_u = linea.precio_u,
  tipo_precio = linea.tipo_precio, margen = linea.margen,
        total = total_producto, paquete = linea.paquete
    WHERE id_cotizacion = icotizacion AND id = linea.id;
   
  END LOOP;
  
  SELECT (flag_factura = true) INTO cambio_servicio FROM cotizaciones WHERE id = icotizacion;

  IF cambio_servicio THEN 
  UPDATE cotizaciones SET flag_factura = iflag_factura WHERE id = icotizacion;

  IF iflag_factura = true THEN
    INSERT INTO logs_cotizacion(id_cotizacion,id_usuario,descripcion,tipo)
    VALUES(icotizacion,iusuario,'Activo el servicio de facturacion','S');

  ELSE
    INSERT INTO logs_cotizacion(id_cotizacion,id_usuario,descripcion,tipo)
    VALUES(icotizacion,iusuario,'Desactivo el servicio de facturacion','S');

  END IF;
  END IF;

  --Generar Log
  INSERT INTO logs_cotizacion(id_cotizacion,id_usuario,descripcion,tipo) 
  VALUES(icotizacion,iusuario,'Realizo cambios a la lista de productos','U');
  
  --Eliminar tablas temporales
  DROP TABLE productos_temp;

  flag       := true;
  mensaje    := 'Se actualizaron correctamente los productos y las variables de cotización';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;


CREATE OR REPLACE FUNCTION eliminar_insumo_cotizacion(
    IN icotizacion integer,
    IN id_insumo integer,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-11-08
-- Descripcion General: Eliminar insumo de la cotización
*****************************************************************************/ 

DECLARE

row_insumo RECORD;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al eliminar insumo de la cotización ';

  SELECT nombre,medidas INTO row_insumo FROM cotizacion_insumos WHERE id=id_insumo AND id_cotizacion=icotizacion;

  UPDATE cotizacion_insumos SET flag=false WHERE id=id_insumo AND id_cotizacion=icotizacion;

  --Generar Log
  INSERT INTO logs_cotizacion(id_cotizacion,id_usuario,descripcion,tipo)
  VALUES(icotizacion,iusuario,'Elimino el insumo: '||row_insumo.nombre,'D');

  flag       := true;
  mensaje    := 'Se elimino correctamente el insumo de la cotización';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;


CREATE OR REPLACE FUNCTION eliminar_producto_cotizacion(
    IN icotizacion integer,
    IN iproducto integer,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-11-06
-- Descripcion General: Eliminar producto de la cotización
*****************************************************************************/ 

DECLARE

row_producto RECORD;
linea RECORD;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al eliminar producto de la cotización ';

  SELECT nombre,medidas INTO row_producto FROM cotizacion_detalle WHERE id=iproducto AND id_cotizacion=icotizacion;

  UPDATE cotizacion_detalle SET flag=false WHERE id=iproducto AND id_cotizacion=icotizacion;

  --Generar Log
  INSERT INTO logs_cotizacion(id_cotizacion,id_usuario,descripcion,tipo)
  VALUES(icotizacion,iusuario,'Elimino el producto: '||row_producto.nombre,'D');

  FOR linea IN SELECT id FROM cotizacion_insumos WHERE id_producto=iproducto AND id_cotizacion=icotizacion LOOP
  PERFORM eliminar_insumo_cotizacion(icotizacion,linea.id,iusuario);
  END LOOP;

  UPDATE cotizacion_insumos SET flag=false WHERE id_producto=iproducto AND id_cotizacion=icotizacion;

  flag       := true;
  mensaje    := 'Se elimino correctamente el producto de la cotización';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;

  CREATE OR REPLACE FUNCTION public.agregar_producto_cotizacion(
    IN icotizacion integer,
    IN inombre character varying,
    IN imedidas character varying,
    IN icantidad integer,
    IN imargen integer,
    IN itipo_precio integer,
    IN iprecio numeric,
    IN ipaquete boolean,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-11-06
-- Descripcion General: Agregar producto a la cotización
*****************************************************************************/ 

DECLARE
total_producto numeric(10,2);
total_neto  numeric(10,2);
total_margen  numeric(10,2);

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al agregar producto a la cotización ';
  
  total_neto := icantidad * iprecio;
  total_margen := total_neto * (imargen / 100);
  total_producto := total_neto + total_margen;
  
  INSERT INTO cotizacion_detalle(id_cotizacion,nombre,medidas,cantidad,tipo_precio,margen,precio_u,total,paquete)
  VALUES (icotizacion,inombre,imedidas,icantidad,itipo_precio,imargen,iprecio,total_producto,ipaquete);

  --Generar Log
  INSERT INTO logs_cotizacion(id_cotizacion,id_usuario,descripcion,tipo)
  VALUES(icotizacion,iusuario,'Se agrego el producto: '||inombre,'C');

  flag       := true;
  mensaje    := 'Se agrego correctamente el producto a la cotización';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER



CREATE OR REPLACE FUNCTION agregar_insumo_cotizacion(
    IN icotizacion integer,
    IN iproducto integer,
    IN iproveedor integer,
    IN itipo_vta character,
    IN inombre character varying,
    IN imedidas character varying,
    IN icantidad integer,
    IN iprecio numeric (10,2),
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-11-08
-- Descripcion General: Agregar insumo a la cotización
*****************************************************************************/ 

DECLARE
total_producto numeric(10,2);

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al agregar insumo a la cotización ';

  total_producto := icantidad * iprecio;

  INSERT INTO cotizacion_insumos(tipo_vta,id_cotizacion,id_proveedor,id_producto,nombre,medidas,cantidad,precio_u,total)
  VALUES (itipo_vta,icotizacion,iproveedor,iproducto,inombre,imedidas,icantidad,iprecio,total_producto);

  --Generar Log
  INSERT INTO logs_cotizacion(id_cotizacion,id_usuario,descripcion,tipo)
  VALUES(icotizacion,iusuario,'Agrego el insumo: '||inombre,'C');

  flag       := true;
  mensaje    := 'Se agrego correctamente el insumo a la cotización';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;




   CREATE OR REPLACE FUNCTION eliminar_producto_cotizacion(
    IN icotizacion integer,
    IN iproducto integer,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-11-06
-- Descripcion General: Eliminar producto de la cotización
*****************************************************************************/ 

DECLARE

row_producto RECORD;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al eliminar producto de la cotización ';

  SELECT nombre,medidas INTO row_producto FROM cotizacion_detalle WHERE id=iproducto AND id_cotizacion=icotizacion;

  UPDATE cotizacion_detalle SET flag=false WHERE id=iproducto AND id_cotizacion=icotizacion;
  
  --Generar Log
  INSERT INTO logs_cotizacion(id_cotizacion,id_usuario,descripcion,tipo)
  VALUES(icotizacion,iusuario,'Elimino el producto: '||row_producto.nombre,'D');

  flag       := true;
  mensaje    := 'Se elimino correctamente el producto de la cotización';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;
  

CREATE OR REPLACE FUNCTION public.editar_servicios_cotizacion(
    IN icotizacion integer,
    IN iusuario integer,
    IN iflag_factura boolean,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2019-01-02
-- Descripcion General: Actualizar servicios de cotización 
*****************************************************************************/ 

DECLARE
numero_pedido integer;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al actualizar los servicios de cotizacion ';

  UPDATE cotizaciones SET flag_factura = iflag_factura WHERE id = icotizacion;

   --Generar Log
  IF iflag_factura = true THEN
    INSERT INTO logs_cotizacion(id_cotizacion,id_usuario,descripcion,tipo)
    VALUES(icotizacion,iusuario,'Activo el servicio de facturacion','S');

  ELSE
    INSERT INTO logs_cotizacion(id_cotizacion,id_usuario,descripcion,tipo)
    VALUES(icotizacion,iusuario,'Desactivo el servicio de facturacion','S');

  END IF;

  flag       := true;
  mensaje    := 'Se actualizaron correctamente los servicios de cotización';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;

CREATE OR REPLACE FUNCTION public.editar_fecha_cotizacion(
    IN icotizacion integer,
    IN ifecha_entrega date,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2019-01-02
-- Descripcion General: Actualizar fecha de cotización 
*****************************************************************************/ 

DECLARE
numero_pedido integer;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al fecha de cotización ';

  UPDATE cotizaciones SET fecha_entrega = ifecha_entrega WHERE id = icotizacion;

   --Generar Log
  INSERT INTO logs_cotizacion(id_cotizacion,id_usuario,descripcion,tipo)
  VALUES(icotizacion,iusuario,'Se actualizo la fecha de entrega para el día: '||ifecha_entrega,'F');

  flag       := true;
  mensaje    := 'Se actualizo correctamente la fecha';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;



CREATE OR REPLACE FUNCTION public.generar_pedido(
    IN icliente integer,
    IN idatos_cliente jsonb,
    IN iproductos jsonb,
    IN iflag_factura boolean,
    IN iflag_medida boolean,
    IN iflag_montaje boolean,
    IN icomentarios character varying,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying,
    OUT id_pedido integer)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-10-05
-- Descripcion General: Esta función se encarga de procesar un pedido
*****************************************************************************/ 

DECLARE
--Variables de trabajo
linea record;
total_producto numeric(10,2);
subtotal_pedido numeric(10,2);
iva_pedido numeric(10,2);
descuento_pedido numeric(10,2);
total_pedido numeric(10,2);
id_nuevo_pedido integer;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al generar la orden';
  id_pedido   := 0;   

  --Eliminar tablas temporales
  DROP TABLE IF EXISTS datos_cliente_temp;
  DROP TABLE IF EXISTS productos_temp;

  --Crear tablas temporales
  CREATE TABLE datos_cliente_temp(
    id integer,
    rfc character varying,
    nombre character varying,
    apellido_paterno character varying,
    apellido_materno character varying,
    email character varying,
    telefono character varying,
    celular character varying,
    calle character varying,
    num_ext character varying,
    num_int character varying,
    colonia character varying,
    entre_calles character varying,
    cod_postal character varying,
    localidad character varying,
    municipio character varying,
    estado character varying,
    credito numeric,
    estatus character(1)
  );  

  CREATE TABLE productos_temp(
    nombre character varying,
    medidas character varying,
    cantidad integer,
    precio_u numeric (10,2),
    total numeric (10,2),
    paquete boolean
  );

  --Obtener Datos Cliente
  INSERT INTO datos_cliente_temp
  SELECT id,rfc,nombre,apellido_paterno,apellido_materno,email,telefono,celular,
    calle,num_ext,num_int,colonia,entre_calles,cod_postal,localidad,municipio,estado,credito,estatus
  FROM jsonb_to_record((idatos_cliente))  
  AS  (id integer,rfc character varying,nombre character varying,apellido_paterno character varying,
    apellido_materno character varying,email character varying,telefono character varying,
    celular character varying,calle character varying,num_ext character varying,num_int character varying,
    colonia character varying,entre_calles character varying,cod_postal character varying,
    localidad character varying,municipio character varying,estado character varying,
    credito numeric,estatus character(1));

  --Obtener Lista de productos
  FOR linea IN SELECT nombre,medidas,cantidad,precio,paquete FROM jsonb_to_recordset((iproductos))  
    AS  (nombre character varying, medidas character varying, cantidad integer, 
    precio numeric (10,2), paquete boolean ) LOOP

    total_producto := linea.cantidad * linea.precio;

    INSERT INTO productos_temp(nombre,medidas,cantidad,precio_u,total,paquete)
    VALUES (linea.nombre,linea.medidas,linea.cantidad,linea.precio,total_producto,linea.paquete);
   
  END LOOP;

  --Obtener subtotal de pedido
  SELECT sum(precio_u * cantidad) INTO subtotal_pedido FROM productos_temp;

  --Sumar IVA en caso de ser factura
  IF iflag_factura = true THEN
  iva_pedido := 0.16 * subtotal_pedido;
  ELSE
  iva_pedido := 0;
  END IF;
  
  --Obtener total de pedido
  descuento_pedido := 0;
  total_pedido = subtotal_pedido + iva_pedido;

  --Crear pedido
  INSERT INTO pedidos(id_cliente,flag_factura,flag_medida,flag_montaje,comentarios,subtotal,descuento,iva,total,id_usuario)
  VALUES(icliente,iflag_factura,iflag_medida,iflag_montaje,icomentarios,subtotal_pedido,descuento_pedido,iva_pedido,total_pedido,iusuario) 
  RETURNING id INTO id_nuevo_pedido;

  --Anexar datos de contacto de pedido
  INSERT INTO pedido_contacto(id_pedido,id_cliente,rfc,nombre,apellido_paterno,
  apellido_materno,email,telefono,celular,calle,num_ext,num_int,colonia,
  entre_calles,cod_postal,localidad,municipio,estado)
  SELECT id_nuevo_pedido,id,rfc,nombre,apellido_paterno,apellido_materno,email,telefono,celular,
   calle,num_ext,num_int,colonia,entre_calles,cod_postal,localidad,municipio,estado  
  FROM datos_cliente_temp;

  --Anexar lista de productos de pedido
  INSERT INTO pedido_detalle(id_pedido,nombre,medidas,cantidad,precio_u,total,paquete)
  SELECT id_nuevo_pedido,nombre,medidas,cantidad,precio_u,total,paquete
  FROM productos_temp;

  --Eliminar tablas temporales
  DROP TABLE productos_temp;
  DROP TABLE datos_cliente_temp;

  --Generar Log Pedido
  INSERT INTO logs_pedido(id_pedido,id_usuario,descripcion,tipo)
  VALUES(id_nuevo_pedido,iusuario,'Creo un nuevo pedido','C');

  --Crear una cotización
  PERFORM generar_cotizacion(id_nuevo_pedido,0,iusuario);

  --Variables de salida
  flag       := true;
  mensaje    := 'Se registro correctamente el pedido';
  id_pedido  := id_nuevo_pedido;   
  RETURN NEXT;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  id_pedido  := 0;  
  RETURN NEXT; 
END
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;

CREATE OR REPLACE FUNCTION generar_cotizacion(
    IN ipedido integer,
    IN iasignacion integer,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying,
    OUT id_cotizacion integer)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-10-17
-- Descripcion General: Esta función se encarga de genenerar una nueva cotización
*****************************************************************************/ 

DECLARE
--Variables de trabajo
facturacion boolean;
comentarios_pedido character varying;
id_nueva_cotizacion integer;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al generar cotizacion';
  id_cotizacion   := 0;   

  IF (SELECT count(id) FROM cotizaciones WHERE id_pedido=ipedido AND asignacion = 0 AND estatus <> 'B' ) > 0 THEN
  
    flag       := false;
    mensaje    := 'Ya hay una cotizacion pendiente por asignar';
    id_cotizacion   := 0; 
    RETURN NEXT;  
  
  ELSE
  
    SELECT flag_factura,comentarios INTO facturacion,comentarios_pedido FROM pedidos WHERE id=ipedido;
    
    INSERT INTO cotizaciones(id_pedido,flag_factura,comentarios,asignacion,id_usuario)
    VALUES (ipedido,facturacion,comentarios_pedido,iasignacion,iusuario) RETURNING id INTO id_nueva_cotizacion;

    INSERT INTO cotizacion_detalle(id_cotizacion,nombre,medidas,cantidad,tipo_precio,margen,precio_u,total,paquete)
    SELECT id_nueva_cotizacion,p.nombre,p.medidas,p.cantidad,1,100,p.precio_u,p.total,p.paquete 
    FROM pedido_detalle AS p WHERE id_pedido=ipedido AND p.flag=true;

    IF (SELECT estatus FROM pedidos WHERE id= ipedido) = 'P' THEN
      UPDATE pedidos SET estatus = 'C' WHERE id = ipedido;
    END IF;

    --Generar Log
    INSERT INTO logs_cotizacion(id_cotizacion,id_usuario,descripcion,tipo)
    VALUES(id_nueva_cotizacion,iusuario,'Creo una nueva cotización','C');

    --Variables de salida
    flag       := true;
    mensaje    := 'Se registro correctamente la cotizacion';
    id_cotizacion  := id_nueva_cotizacion;   
    RETURN NEXT;
  END IF;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  id_cotizacion  := 0;  
  RETURN NEXT; 
END
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;


  CREATE OR REPLACE FUNCTION reclamar_cotizacion(
    IN icotizacion integer,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-10-29
-- Descripcion General: Asignar cotización a usuario
*****************************************************************************/ 

DECLARE

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al asignar cotizacion ';

  --Se inserta el equipo
  UPDATE cotizaciones 
  SET asignacion=iusuario
  WHERE id=icotizacion;

  --Generar Log
  INSERT INTO logs_cotizacion(id_cotizacion,id_usuario,descripcion,tipo)
  VALUES(icotizacion,iusuario,'Reclamo la cotización','R');

  flag       := true;
  mensaje    := 'Se asigno el usuario correctamente';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;

 CREATE OR REPLACE FUNCTION public.eliminar_cotizacion(
    IN icotizacion integer,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-11-05
-- Descripcion General: Eliminar cotización a usuario
*****************************************************************************/ 

DECLARE
numero_pedido integer;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al eliminar cotizacion ';

  SELECT id_pedido INTO numero_pedido FROM cotizaciones WHERE id=icotizacion;

  --Verificar que el pedido tenga estatus "PENDIENTE DE COTIZAR" O "COTIZANDO"
  IF (SELECT estatus FROM pedidos WHERE id = numero_pedido ) IN  ('C','P') THEN
  
    IF (SELECT count(*) FROM cotizaciones WHERE id_pedido = numero_pedido AND estatus = 'P') <= 1 THEN
      flag       := false;
      mensaje    := 'No se puede eliminar la cotización, debido a que es la unica ';
      RETURN NEXT;
    ELSE 

     UPDATE cotizaciones  SET estatus='B' WHERE id=icotizacion;
     
      --Generar Log
      INSERT INTO logs_cotizacion(id_cotizacion,id_usuario,descripcion,tipo)
      VALUES(icotizacion,iusuario,'Elimino la cotización','D');
      
      flag       := true;
      mensaje    := 'Se elimino la cotizacion correctamente';
      RETURN NEXT;
    END IF;
  ELSE
    UPDATE cotizaciones  SET estatus='B' WHERE id=icotizacion;

    --Generar Log
    INSERT INTO logs_cotizacion(id_cotizacion,id_usuario,descripcion,tipo)
    VALUES(icotizacion,iusuario,'Elimino la cotización','D');

    flag       := true;
    mensaje    := 'Se elimino la cotizacion correctamente';
    RETURN NEXT;
  END IF;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;