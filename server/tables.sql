﻿DROP TABLE logs_cotizacion;
DROP TABLE logs_nota;
DROP TABLE logs_cortes;
DROP TABLE logs_pedido;
DROP TABLE logs_movimientos;
DROP TABLE logs_tipos_pagos;
DROP TABLE logs_clientes;
DROP TABLE logs_paquetes;
---DROP TABLE logs_equipos;
---DROP TABLE paquetes;
DROP TABLE incidencias_apertura_corte;
DROP TABLE incidencias_cierre_corte;
DROP TABLE nota_detalle;
DROP TABLE pedido_detalle;
DROP TABLE pedido_contacto;
DROP TABLE cotizacion_insumos;
DROP TABLE cotizacion_detalle;
DROP TABLE cotizacion_repuestas;
DROP TABLE notas;
DROP TABLE cotizaciones;
DROP TABLE pedidos;
DROP TABLE movimientos;
DROP TABLE cortes;
---DROP TABLE cat_tipos_pagos;
---DROP TABLE cat_metodos_pago;
---DROP TABLE clientes_contado;
---DROP TABLE reparaciones_equipos;
---DROP TABLE servicios_equipos
---DROP TABLE inventario_equipos;
---DROP TABLE clientes;
---DROP TABLE precios;
---DROP TABLE proveedores;
---DROP TABLE users;


CREATE TABLE users
(
  id serial PRIMARY KEY NOT NULL,
  username character varying(32) NOT NULL,
  password text NOT NULL,
  name character varying(100) NOT NULL,
  last_name character varying(100) NOT NULL,
  email character varying(100),
  profile character varying(30) NOT NULL,
  verificationtoken text,
  register timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);


CREATE TABLE precios(
 id serial PRIMARY KEY,
 descripcion character varying(100) NOT NULL,
 margen integer NOT NULL,
 registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

---INSERT INTO precios(descripcion,margen) VALUES('PUBLICO EN GENERAL',100);
---INSERT INTO precios(descripcion,margen) VALUES('MAQUILA',75);


CREATE TABLE proveedores
(
  id serial PRIMARY KEY,
  id_tipo_pago integer NOT NULL DEFAULT 0,
  rfc character(15) NOT NULL,
  nombre character varying(100) NOT NULL,
  nombre_comercial character varying(100) NOT NULL,
  email character varying(100) NOT NULL,
  telefono character varying(10) NOT NULL,
  calle character varying(100) NOT NULL,
  num_ext integer NOT NULL,
  num_int integer DEFAULT 0,
  colonia character varying(100) NOT NULL,
  cod_postal character varying(10) NOT NULL,
  localidad character varying(50) NOT NULL,
  municipio character varying(50) NOT NULL,
  estado character varying(50) NOT NULL,
  credito numeric(10,2) DEFAULT 0.00,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now()),
  flag boolean DEFAULT true
);


CREATE TABLE clientes
(
  id serial PRIMARY KEY,
  rfc character(15) NOT NULL,
  nombre character varying(200) NOT NULL,
  apellido_paterno character varying(200) NOT NULL,
  apellido_materno character varying(200),
  nombre_comercial character varying(300) NOT NULL,
  email character varying(200) NOT NULL,
  telefono character varying(10),
  celular character varying(10) NOT NULL,
  calle character varying(200),
  num_ext character varying(10),
  num_int character varying(10),
  colonia character varying(200),
  entre_calles character varying(300),
  cod_postal character varying(10),
  localidad character varying(50),
  municipio character varying(50),
  estado character varying(50),
  credito numeric(10,2) DEFAULT 0,
  tipo_precio integer REFERENCES precios(id),
  estatus character(1) DEFAULT 'C',
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now()),
  flag boolean DEFAULT true
);  


CREATE TABLE logs_clientes (
  id serial PRIMARY KEY NOT NULL,
  id_cliente integer REFERENCES clientes(id),
  usuario integer NOT NULL DEFAULT 0,
  descripcion character varying(300) NOT NULL,
  tipo character(1) NOT NULL,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE inventario_equipos
(
  id serial PRIMARY KEY,
  nombre character varying(200) NOT NULL,
  tipo character varying(100) NOT NULL,
  descripcion character varying(800),
  fecha_compra date, 
  flag boolean DEFAULT true,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE reparaciones_equipos(
  id serial PRIMARY KEY,
  id_equipo integer REFERENCES inventario_equipos(id) NOT NULL,
  id_usuario integer REFERENCES users(id) NOT NULL,
  id_proveedor integer REFERENCES proveedores(id) NOT NULL,
  descripcion_falla character varying(800) NOT NULL,
  id_usuario_rep integer REFERENCES users(id),
  descripcion_reparacion character varying(800),
  total numeric(10,2) DEFAULT 0.00,
  fecha date, 
  flag boolean DEFAULT false,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE servicios_equipos(
  id serial PRIMARY KEY,
  id_equipo integer REFERENCES inventario_equipos(id) NOT NULL,
  id_usuario integer REFERENCES users(id) NOT NULL,
  id_proveedor integer REFERENCES proveedores(id) NOT NULL,
  descripcion_servicio character varying(800) NOT NULL,
  id_usuario_serv integer REFERENCES users(id),
  total numeric(10,2) DEFAULT 0.00,
  fecha date, 
  flag boolean DEFAULT false,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);          

CREATE TABLE logs_equipos (
  id serial PRIMARY KEY NOT NULL,
  id_equipo integer REFERENCES inventario_equipos(id),
  usuario integer NOT NULL DEFAULT 0,
  descripcion character varying(300) NOT NULL,
  tipo character(1) NOT NULL,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE cat_metodos_pago
(
  id serial PRIMARY KEY,
  nombre character varying(300),
  visible boolean DEFAULT true,
  flag boolean DEFAULT true,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

---INSERT INTO cat_metodos_pago(nombre) VALUES('EFECTIVO');
---INSERT INTO cat_metodos_pago(nombre) VALUES('TARJETA DE DEBITO');
---INSERT INTO cat_metodos_pago(nombre) VALUES('TARJETA DE CREDITO');
---INSERT INTO cat_metodos_pago(nombre) VALUES('TRANSFERENCIA');
---INSERT INTO cat_metodos_pago(nombre) VALUES('CHEQUE');
---INSERT INTO cat_metodos_pago(nombre) VALUES('DEPOSITO EN EFECTIVO');


CREATE TABLE paquetes
(
  id serial PRIMARY KEY NOT NULL,
  nombre character varying NOT NULL,
  medidas character varying,
  cantidad integer NOT NULL DEFAULT 1,
  precio numeric(10,2) NOT NULL,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now()),
  flag boolean DEFAULT true
);

CREATE TABLE logs_paquetes (
  id serial PRIMARY KEY NOT NULL,
  id_paquete integer REFERENCES paquetes(id),
  usuario integer NOT NULL DEFAULT 0,
  descripcion character varying(300) NOT NULL,
  tipo character(1) NOT NULL,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE clientes_contado
(
  id serial PRIMARY KEY,
  rfc character(15) NOT NULL,
  nombre character varying(200) NOT NULL,
  apellido_paterno character varying(200) NOT NULL,
  apellido_materno character varying(200),
  nombre_comercial character varying(300),
  email character varying(200) NOT NULL,
  telefono character varying(10),
  celular character varying(10) NOT NULL,
  calle character varying(200),
  num_ext character varying(10),
  num_int character varying(10),
  colonia character varying(200),
  entre_calles character varying(300),
  cod_postal character varying(10),
  localidad character varying(50),
  municipio character varying(50),
  estado character varying(50),
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now()),
  flag boolean DEFAULT true
);   



CREATE TABLE cat_tipos_pagos
(
  id serial PRIMARY KEY,
  nombre character varying(300),
  visible boolean DEFAULT true,
  flag boolean DEFAULT true,
  movimiento character(1) NOT NULL DEFAULT 'C',
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

---INSERT INTO cat_tipos_pagos(nombre,movimiento,visible) VALUES('GASTOS EN GENERAL','C',true);
---INSERT INTO cat_tipos_pagos(nombre,movimiento,visible) VALUES('FALTANTE','C',false);
---INSERT INTO cat_tipos_pagos(nombre,movimiento,visible) VALUES('SOBRANTE','C',false);
---INSERT INTO cat_tipos_pagos(nombre,movimiento,visible) VALUES('INSUMOS','C',true);
---INSERT INTO cat_tipos_pagos(nombre,movimiento,visible) VALUES('NOMINA','C',true);
---INSERT INTO cat_tipos_pagos(nombre,movimiento,visible) VALUES('ABONO NOTA DE VENTA','A',false);
---INSERT INTO cat_tipos_pagos(nombre,movimiento,visible) VALUES('ABONO NOTA DE VENTA (SISTEMA ANTERIOR)','A',true);


CREATE TABLE logs_tipos_pagos (
  id serial PRIMARY KEY NOT NULL,
  id_tipo integer REFERENCES cat_tipos_pagos(id),
  id_usuario integer REFERENCES users(id),
  descripcion character varying(300) NOT NULL,
  tipo character(1) NOT NULL,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);


/*
  Estatus:
  A - Apertura
  C - Cierre
  D - Cancelado
  E - Eliminado
*/
CREATE TABLE cortes
(
  id serial PRIMARY KEY NOT NULL,
  id_usuario integer REFERENCES users(id) NOT NULL,
  saldo_apertura numeric(10,2) NOT NULL,
  saldo_cierre numeric(10,2),
  diferencia numeric(10,2),
  saldo_final numeric(10,2),
  estatus character(1) NOT NULL DEFAULT 'A',
  fecha date NOT NULL DEFAULT timezone('America/Mazatlan'::text, now())::date,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE incidencias_apertura_corte
(
  id serial PRIMARY KEY NOT NULL,
  id_corte integer REFERENCES cortes(id),
  id_usuario integer REFERENCES users(id),
  motivo character varying(300) NOT NULL,
  movimiento character(1) NOT NULL,
  importe numeric(10,2) NOT NULL,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);


CREATE TABLE incidencias_cierre_corte
(
  id serial PRIMARY KEY NOT NULL,
  id_corte integer REFERENCES cortes(id),
  id_usuario integer REFERENCES users(id),
  motivo character varying(300) NOT NULL,
  movimiento character(1) NOT NULL,
  importe numeric(10,2) NOT NULL,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);


/* CRUD */ 
CREATE TABLE logs_cortes
(
  id serial PRIMARY KEY NOT NULL,
  id_corte integer REFERENCES cortes(id),
  id_usuario integer REFERENCES users(id),
  descripcion character varying(300) NOT NULL,
  tipo character(1) NOT NULL,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

/*
  Estatus:
  R - Realizado
  P - Pendiente de Aplicar
  C - Cancelado
  E - Eliminado

  Movimientos:
  C - Cargo
  A - Abono

  Metodo:

*/


CREATE TABLE movimientos
(
  id serial PRIMARY KEY NOT NULL,
  id_corte integer REFERENCES cortes(id) NOT NULL,
  id_proveedor integer NOT NULL DEFAULT 0,
  id_usuario integer REFERENCES users(id) NOT NULL,
  documento integer NOT NULL DEFAULT 0,
  concepto character varying(300) NOT NULL,
  importe numeric(10,2) NOT NULL,
  metodo integer REFERENCES cat_metodos_pago(id) NOT NULL,
  tipo integer REFERENCES cat_tipos_pagos(id) NOT NULL,
  movimiento character(1) NOT NULL,
  estatus character(1) NOT NULL DEFAULT 'R',
  fecha date NOT NULL DEFAULT timezone('America/Mazatlan'::text, now())::date,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

/* CRUD */ 
CREATE TABLE logs_movimientos
(
  id serial PRIMARY KEY NOT NULL,
  id_movimiento integer REFERENCES movimientos(id),
  id_usuario integer REFERENCES users(id),
  descripcion character varying(300) NOT NULL,
  tipo character(1) NOT NULL,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);




/*
Estatus: 
P - Pendiente de Cotizar
C - Cotizando
V - Venta
R - Venta Rapida

Origen:
S - Sucursal
*/

CREATE TABLE pedidos(
  id serial PRIMARY KEY,
  id_cliente integer NOT NULL,
  flag_factura boolean NOT NULL,
  flag_medida boolean NOT NULL,
  flag_montaje boolean NOT NULL,
  comentarios character varying(300),
  subtotal numeric(10,2) NOT NULL DEFAULT 0,
  descuento numeric(10,2) NOT NULL DEFAULT 0,
  iva numeric(10,2) NOT NULL DEFAULT 0,
  total numeric(10,2) NOT NULL DEFAULT 0,
  estatus character(1) NOT NULL DEFAULT 'P',
  origen character(1) NOT NULL DEFAULT 'S',
  cotizacion integer DEFAULT 0,
  id_usuario integer REFERENCES users(id) NOT NULL,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE logs_pedido
(
  id serial PRIMARY KEY NOT NULL,
  id_pedido integer REFERENCES pedidos(id),
  id_usuario integer REFERENCES users(id),
  descripcion character varying(300) NOT NULL,
  tipo character(1) NOT NULL,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE pedido_contacto
(
    id serial PRIMARY KEY,
    id_pedido integer REFERENCES pedidos(id) NOT NULL,
    id_cliente integer,
    rfc character varying,
    nombre character varying,
    apellido_paterno character varying,
    apellido_materno character varying,
    nombre_comercial character varying(300),
    email character varying,
    telefono character varying,
    celular character varying,
    calle character varying,
    num_ext character varying,
    num_int character varying,
    colonia character varying,
    entre_calles character varying,
    cod_postal character varying,
    localidad character varying,
    municipio character varying,
    estado character varying,
    registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE pedido_detalle
(
  id serial PRIMARY KEY,
  id_pedido integer REFERENCES pedidos(id) NOT NULL,
  nombre character varying,
  medidas character varying DEFAULT 'N/A',
  cantidad integer DEFAULT 0,
  precio_u numeric (10,2) DEFAULT 0,
  total numeric (10,2) DEFAULT 0,
  paquete boolean DEFAULT false,
  flag boolean DEFAULT true,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);


/*
Estatus: 
P - Pendiente
E - Elimininada

F - Finalizada

R - Rechazada 
A - Aceptada

C - Cancelada
V - Venta
*/
 
CREATE TABLE cotizaciones(
 id serial PRIMARY KEY,
 id_pedido integer REFERENCES pedidos(id) NOT NULL,
 comentarios character varying(300),
 flag_factura boolean NOT NULL DEFAULT false,
 estatus character(1) NOT NULL DEFAULT 'P',
 asignacion integer NOT NULL DEFAULT 0,
 id_usuario integer REFERENCES users(id) NOT NULL,
 fecha_entrega date,
 registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE logs_cotizacion
(
  id serial PRIMARY KEY NOT NULL,
  id_cotizacion integer REFERENCES cotizaciones(id),
  id_usuario integer REFERENCES users(id),
  descripcion character varying(300) NOT NULL,
  tipo character(1) NOT NULL,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);


CREATE TABLE cotizacion_detalle
(
  id serial PRIMARY KEY,
  id_cotizacion integer REFERENCES cotizaciones(id) NOT NULL,
  nombre character varying,
  medidas character varying DEFAULT 'N/A',
  cantidad integer DEFAULT 0,
  tipo_precio integer REFERENCES precios(id),
  margen integer DEFAULT 0,
  precio_u numeric (10,2) DEFAULT 0,
  total numeric (10,2) DEFAULT 0,
  paquete boolean DEFAULT false,
  flag boolean DEFAULT true,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE cotizacion_insumos
(
  id serial PRIMARY KEY,
  tipo_vta character(1) NOT NULL DEFAULT 'C',
  id_cotizacion integer REFERENCES cotizaciones(id) NOT NULL,
  id_proveedor integer REFERENCES proveedores(id) NOT NULL,
  id_producto integer REFERENCES cotizacion_detalle(id) NOT NULL,
  nombre character varying,
  medidas character varying DEFAULT 'N/A',
  cantidad integer DEFAULT 0,
  precio_u numeric (10,2) DEFAULT 0,
  descuento integer NOT NULL DEFAULT 0,
  total numeric (10,2) DEFAULT 0,
  flag boolean DEFAULT true,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE cotizacion_repuestas(
  id serial PRIMARY KEY,
  id_cotizacion integer REFERENCES cotizaciones(id) NOT NULL,
  comentarios character varying(300),
  contacto character varying(300),
  medio character varying(300), 
  tipo character(1) NOT NULL,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE notas
(
  id serial PRIMARY KEY,
  id_cotizacion integer NOT NULL DEFAULT 0,
  id_pedido integer REFERENCES pedidos(id) NOT NULL,
  id_cliente integer REFERENCES clientes(id),
  rfc character(15) NOT NULL,
  nombre_cliente character varying(200) NOT NULL,
  nombre_comercial character varying(300),
  email character varying(200) NOT NULL,
  telefono character varying(10),
  celular character varying(10) ,
  calle character varying(100),
  num_ext character varying(20),
  num_int character varying(20),
  colonia character varying(100),
  entre_calles character varying(150),
  cod_postal character varying(10),
  localidad character varying(100),
  municipio character varying(100),
  estado character varying(100),
  tipo_vta character(1) NOT NULL DEFAULT 'C',
  subtotal numeric (10,2) NOT NULL DEFAULT 0,
  descuento numeric (10,2) NOT NULL DEFAULT 0,
  iva numeric (10,2) NOT NULL DEFAULT 0,
  total numeric (10,2) NOT NULL DEFAULT 0,
  saldo numeric (10,2) NOT NULL DEFAULT 0,
  fecha_vigencia date,
  flag_factura boolean NOT NULL DEFAULT false,
  estatus character(1) NOT NULL DEFAULT 'A',
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE logs_nota
(
  id serial PRIMARY KEY NOT NULL,
  id_nota integer REFERENCES notas(id),
  id_usuario integer REFERENCES users(id),
  descripcion character varying(300) NOT NULL,
  tipo character(1) NOT NULL,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE nota_detalle
(
  id serial PRIMARY KEY,
  id_nota integer REFERENCES notas(id) NOT NULL,
  nombre character varying,
  medidas character varying DEFAULT 'N/A',
  cantidad integer DEFAULT 0,
  precio_u numeric (10,2) DEFAULT 0,
  total numeric (10,2) DEFAULT 0,
  paquete boolean DEFAULT false,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);



/**
Estatus
P - Pendiente de Pago
L - Liquidada
E - Eliminada
**/

CREATE TABLE orden_pago_proveedores
(
  id serial PRIMARY KEY NOT NULL,
  id_proveedor integer NOT NULL DEFAULT 0,
  id_usuario integer REFERENCES users(id) NOT NULL,
  concepto character varying(300) NOT NULL,
  importe numeric (10,2) NOT NULL DEFAULT 0,
  saldo numeric (10,2) NOT NULL DEFAULT 0,
  tipo_vta character(1) NOT NULL DEFAULT 'D',
  estatus character(1) NOT NULL DEFAULT 'P',
  fecha date NOT NULL DEFAULT timezone('America/Mazatlan'::text, now())::date,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);


